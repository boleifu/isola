//
//  PlayerAI.swift
//  Isola
//
//  Created by Bolei Fu on 10/10/15.
//  Copyright © 2015 boleifu. All rights reserved.
//

import Foundation

protocol PlayerAI {
    
    var name:String {get};
    var role:Role {get}; // P1 or P2
    
    func getNextMove(_ gameBoardState:GameBoardState) -> GridCoordinate;
    func getNextSteal(_ gameBoardState:GameBoardState) -> GridCoordinate;
}

enum PlayerAIEnum {
    case human;
    case easy_AI;
    case hard_AI;
    
    func getPlayerAI(_ role:Role) -> PlayerAI? {
        switch self {
        case PlayerAIEnum.human:
            return nil;
        case PlayerAIEnum.easy_AI:
            return EasyAI(role: role);
        case PlayerAIEnum.hard_AI:
            return HardAI(role: role);
        }
    }
    
    static func findPlayerAI(_ aiName:String) -> PlayerAIEnum {
        if (aiName == GameConstants.EASY_AI) {
            return PlayerAIEnum.easy_AI
        } else if (aiName == GameConstants.HARD_AI) {
            return PlayerAIEnum.hard_AI
        } else {
            return PlayerAIEnum.human;
        }
    }
}
