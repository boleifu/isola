//
//  GameCore.swift
//  Isola
//
//  Created by Bolei Fu on 10/7/15.
//  Copyright © 2015 boleifu. All rights reserved.
//

import SpriteKit

//
// The Game Processer
// This contains all gaming logic and process player moves
//
class GameCore {
    let theme:Theme;
    unowned let gameBoard:GameBoard;
    unowned let hudBoard:HudBoard;
    unowned let playerStatusHUDBoard:BottomHudBoard
    
    let gameOverSoundAction:SKAction;
    let moveSoundAction:SKAction;
    let removeSoundAction:SKAction;
    let invalidMoveSoundAction:SKAction;
    
    // Game Players.
    unowned let gamePlayer1:GamePlayer;
    unowned let gamePlayer2:GamePlayer;
    var player1IconName:String = "P1";
    var player2IconName:String = "P2";
    
    // Game status
    var currentGameStage:GameStage = GameStage.not_START;
    var currentPlayer:GamePlayer? = nil;
    var isWaitingForHumanInput:Bool = true;
    var player1WinNum:Int = 0;
    var player2WinNum:Int = 0;
    
    // Online Mode
    let isOnlineMode:Bool
    
    init(gameBoard:GameBoard, hudBoard:HudBoard, playerStatusHUDBoard:BottomHudBoard, player1:GamePlayer, player2:GamePlayer, theme: Theme, onlineMode:Bool) {
        self.gameBoard = gameBoard;
        self.hudBoard = hudBoard;
        self.playerStatusHUDBoard = playerStatusHUDBoard
        self.gamePlayer1 = player1;
        self.gamePlayer2 = player2;
        self.theme = theme;
        self.isOnlineMode = onlineMode;
        
        player1IconName = PlayerIconUtils.getIconName(player1.playerIconIndex).capitalized;
        player2IconName = PlayerIconUtils.getIconName(player2.playerIconIndex).capitalized;
        
        gameOverSoundAction = SKAction.playSoundFileNamed(theme.gameOverSoundName, waitForCompletion: true);
        moveSoundAction = SKAction.playSoundFileNamed(theme.moveSoundName, waitForCompletion: true);
        removeSoundAction = SKAction.playSoundFileNamed(theme.removeSoundName, waitForCompletion: true);
        invalidMoveSoundAction = SKAction.playSoundFileNamed(theme.invalidMoveSoundName, waitForCompletion: true);
        
        // Init game state
        hudBoard.updateGameStatus(GameConstants.NO_MESSAGE);
        playerStatusHUDBoard.updateGameStage(GameStage.getDescription(currentGameStage));
        
        if isOnlineMode {
            initOnlineGame()
        }
        
        //print("Player 1 name: \(self.player1Name)");
        //print("Player 2 name: \(self.player2Name)");
    }
    
    fileprivate func initOnlineGame() {
        // Change to P1's turn directly.
        evaluate()
        
    }
    
    // Update the game with user input.
    // Return true if input processed, otherwise return false.
    func updateByHuman(_ inputGrid:GridCoordinate) -> Bool{
        if (!isWaitingForHumanInput) {
            print("Warn: Not Human Player's turn yet.");
            return false;
        }
        
        if (currentGameStage == GameStage.not_START) {
            if isOnlineMode {
                // Return false since online game should start automatically.
                return false
            }
            print("Game Started...");
        } else if (currentGameStage == GameStage.p1_WIN || currentGameStage == GameStage.p2_WIN) {
            print("Game Over...");
            if isOnlineMode {
                // Return false online game has only one match.
                return false
            }
            resetTheGame(resetScore: false);
            return true;
        } else {
            print("Got update from Human input \(inputGrid.toString()). Current stage = \(currentGameStage)");
            let desiredStatus:GridStatus = getDesiredStatus(currentGameStage);
            if (gameBoard.isMoveValid(inputGrid, desiredStatus:desiredStatus)) {
                processTurn(inputGrid: inputGrid, desiredStatus:desiredStatus);
                
                // Play SFX
                if (GameConfigUtils.isSFXEnabled()) {
                    // SFX is enabled
                    if (desiredStatus == GridStatus.p1 || desiredStatus == GridStatus.p2) {
                        gameBoard.run(moveSoundAction);
                    } else if (desiredStatus == GridStatus.removed) {
                        gameBoard.run(removeSoundAction);
                    }
                }
            } else {
                print("Invalid Move on \(inputGrid.toString()). Desired move: \(desiredStatus)")
                hudBoard.updateGameStatus(GameConstants.INVALID_MOVE);
                if (GameConfigUtils.isSFXEnabled()) {
                    gameBoard.run(invalidMoveSoundAction)
                }
                return false;
            }
        }
        
        // Remove hilighted move
        gameBoard.removeHilightedMove();
        
        evaluate();
        return true;
    }
    
    // Do the AI move if it is AI's turn, triggered each frame
    func processAiTurn() {
        // No nothing for the non-AI turn
        if (currentPlayer != nil) {
            let nextAiMove:GridCoordinate? = getNextAiMove(currentPlayer!.playerAI);
            if (nextAiMove == nil) {
                print("Error: Cannot get move from AI \(String(describing: currentPlayer!.playerAI?.name)) for stage \(currentGameStage).");
                return;
            } else {
                // Process the AI move
                print("Got update from AI input \(nextAiMove!.toString()). Current stage = \(currentGameStage)");
                // No validation needed for the AI move. Process the move directly.
                let desiredStatus:GridStatus = getDesiredStatus(currentGameStage);
                processTurn(inputGrid: nextAiMove!, desiredStatus:desiredStatus);
                
                evaluate();
            }
        }
    }
    
    fileprivate func getNextAiMove(_ aiPlayer:PlayerAI?) -> GridCoordinate?{
        if (aiPlayer == nil) {
            print("Error: Not an AI player.")
            return nil;
        }
        
        if (currentGameStage == GameStage.p1_MOVE || currentGameStage == GameStage.p2_MOVE) {
            return aiPlayer?.getNextMove(gameBoard.getBoardStat());
        } else if (currentGameStage == GameStage.p1_REMOVE || currentGameStage == GameStage.p2_REMOVE) {
            return aiPlayer?.getNextSteal(gameBoard.getBoardStat());
        } else {
            // AI only do move or remove
            return nil;
        }
    }
    
    fileprivate func processTurn(inputGrid:GridCoordinate, desiredStatus:GridStatus) {
        gameBoard.procssMove(inputGrid, desiredStatus:desiredStatus);

        let currentTurnMsg:String = GameStage.getTurnMsg(currentGameStage);
        if (currentGameStage.isPlayer1Stage()) {
            hudBoard.addGameMsg(msg: String(format: currentTurnMsg, arguments: [player1IconName, inputGrid.toString()]), role: Role.p1);
        } else if (currentGameStage.isPlayer2Stage()) {
            hudBoard.addGameMsg(msg: String(format: currentTurnMsg, arguments: [player2IconName, inputGrid.toString()]), role: Role.p2);
        } else {
            hudBoard.addGameMsg(msg: currentTurnMsg, role: nil);
        }
    }
    
    // Evalueate the current game state and process correspondingly
    fileprivate func evaluate() {
        // Evaluate the current game status
        let gameOverStage:GameStage? = checkWinningCondition();
        if (gameOverStage != nil) {
            // Game Over
            currentGameStage = gameOverStage!;
            if (GameConfigUtils.isSFXEnabled()) {
                // SFX enabled
                gameBoard.run(gameOverSoundAction);
            }
        } else {
            // No player win
            // Game contiue, and go to the next stage
            currentGameStage = currentGameStage.nextState();
        }
        
        // Get the player for the next stage
        currentPlayer = getCurrentPlayer();
        
        // Check whether needs wait for player's input.
        if currentPlayer == nil {
            // No active player, waiting for Human player's input.
            isWaitingForHumanInput = true;
        } else {
            if isOnlineMode {
                // Waiting for Human player's input if the current player is self.
                isWaitingForHumanInput = !currentPlayer!.isOnline;
            } else {
                // Offline Mode
                // Waiting for Human player's input if the current player is not AI.
                isWaitingForHumanInput = (currentPlayer!.playerAI == nil)
            }
        }
        
        if isWaitingForHumanInput {
            print("Human player's turn.")
            // Hilight the next move
            if (GameConfigUtils.isMoveHintEnabled()) {
                // Move Hint enabled
                gameBoard.hilightAvailableMove(getDesiredStatus(currentGameStage));
            }
        } else {
            print("AI/Online Opponent's turn.")
        }
        
        // Print current stage desc to the hud
        let currentStageDescFormat:String = GameStage.getDescription(currentGameStage);
        if (currentGameStage.isPlayer1Stage()) {
            playerStatusHUDBoard.updateGameStage(String(format: currentStageDescFormat, arguments: [player1IconName]));
            playerStatusHUDBoard.setActivePlayer(role: Role.p1)
        } else if (currentGameStage.isPlayer2Stage()) {
            playerStatusHUDBoard.updateGameStage(String(format: currentStageDescFormat, arguments: [player2IconName]));
            playerStatusHUDBoard.setActivePlayer(role: Role.p2)
        } else {
            playerStatusHUDBoard.updateGameStage(currentStageDescFormat);
            playerStatusHUDBoard.setActivePlayer(role: nil)
        }
        
        // Handling Winning stage
        let currentTurnMsg:String = GameStage.getTurnMsg(currentGameStage);
        if currentGameStage == GameStage.p1_WIN {
            player1WinNum += 1
            refreshGameScore()
            hudBoard.addGameMsg(msg: String(format: currentTurnMsg, arguments: [player1IconName]), role: Role.p1);
        } else if currentGameStage == GameStage.p2_WIN {
            player2WinNum += 1
            refreshGameScore()
            hudBoard.addGameMsg(msg: String(format: currentTurnMsg, arguments: [player2IconName]), role: Role.p2);
        }
        
    }
    
    // Get the player of the current game stage
    fileprivate func getCurrentPlayer() -> GamePlayer? {
        if (currentGameStage == GameStage.p1_MOVE || currentGameStage == GameStage.p1_REMOVE) {
            return self.gamePlayer1;
        } else if (currentGameStage == GameStage.p2_MOVE || currentGameStage == GameStage.p2_REMOVE) {
            return self.gamePlayer2;
        }
        
        return nil
    }
    
    // Set the game to the start state
    func resetTheGame(resetScore:Bool) {
        print("Starting the game...");
        gameBoard.resetGameBoard();
        currentGameStage = GameStage.not_START;
        playerStatusHUDBoard.updateGameStage(GameStage.getDescription(currentGameStage));
        hudBoard.clearGameMsg()
        if resetScore {
            player1WinNum = 0
            player2WinNum = 0
            refreshGameScore()
        }
    }
    
    // Refresh the game score in the HUD board.
    func refreshGameScore() {
        if !isOnlineMode {
            // Only has score board in offline mode
            playerStatusHUDBoard.refreshScoreBoard(p1Score: player1WinNum, p2Score: player2WinNum)
        }
    }
    
    // Over the game if one player lost
    fileprivate func checkWinningCondition() -> GameStage?{
        // Check P1 lost
        let p1Grids:Array<BoardGrid> = gameBoard.getGridWithStatus(GridStatus.p1);
        for grid:BoardGrid in p1Grids {
            if (gameBoard.isSurrounded(grid.coordinate)) {
                return GameStage.p2_WIN;
            }
        }
        
        // Check P2 lost
        let p2Grids:Array<BoardGrid> = gameBoard.getGridWithStatus(GridStatus.p2);
        for grid:BoardGrid in p2Grids {
            if (gameBoard.isSurrounded(grid.coordinate)) {
                return GameStage.p1_WIN;
            }
        }
        
        // No player lost
        return nil;
    }
    
    // Get the corresponding grid status desired for the given game stage
    fileprivate func getDesiredStatus(_ targetStage:GameStage) -> GridStatus {
        switch(targetStage) {
        case GameStage.p1_MOVE:
            return GridStatus.p1;
        case GameStage.p1_REMOVE:
            return GridStatus.removed;
        case GameStage.p2_MOVE:
            return GridStatus.p2;
        case GameStage.p2_REMOVE:
            return GridStatus.removed;
        default:
            return GridStatus.idle;
        }
    }
    
    // Online only
    
    // Process opponent's game play, triggered each frame in online mode.
    // Return true if the play is accepted, otherwise return false.
    func processOpponentPlay(input: GridCoordinate) -> Bool {
        // No nothing if not opponent's turn.
        if (!isOnlineMode || isWaitingForHumanInput) {
            return false
        }
        
        if currentPlayer == nil {
            return false
        }
        
        print("Got update from online opponent \(input). Current stage = \(currentGameStage)");
        // No validation needed for online opponent's move. Process the move directly.
        let desiredStatus:GridStatus = getDesiredStatus(currentGameStage);
        processTurn(inputGrid: input, desiredStatus:desiredStatus);

        // Play SFX
        if (GameConfigUtils.isSFXEnabled()) {
            // SFX is enabled
            if (desiredStatus == GridStatus.p1 || desiredStatus == GridStatus.p2) {
                gameBoard.run(moveSoundAction);
            } else if (desiredStatus == GridStatus.removed) {
                gameBoard.run(removeSoundAction);
            }
        }
        
        evaluate();
        return true
    }
    
    // Return the self game player.
    func getSelfPlayer() -> GamePlayer {
        if self.gamePlayer1.isOnline {
            return self.gamePlayer2
        }
        return self.gamePlayer1
    }
    
    // Return the online opponent game player.
    func getOpponentPlayer() -> GamePlayer {
        if self.gamePlayer1.isOnline {
            return self.gamePlayer1
        }
        return self.gamePlayer2
    }
    
    // Return 0 if no result, 1 self win, and -1 opponent win.
    func checkOnlineGameResult() -> Int {
        if currentGameStage == GameStage.p1_WIN {
            if gamePlayer1.isOnline {
                return -1
            } else {
                return 1
            }
        } else if currentGameStage == GameStage.p2_WIN {
            if gamePlayer2.isOnline {
                return -1
            } else {
                return 1
            }
        }
        
        return 0
    }
    
    func isSelfTurn() -> Bool {
        if currentGameStage.isPlayer1Stage() {
            return !gamePlayer1.isOnline
        } else if currentGameStage.isPlayer1Stage() {
            return !gamePlayer2.isOnline
        }

        // Not a game play turn
        return false
    }
    
    
}
