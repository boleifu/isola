//
//  BoardGrid.swift
//  Isola
//
//  Created by Bolei Fu on 10/6/15.
//  Copyright © 2015 boleifu. All rights reserved.
//

import SpriteKit

//
// This class one grid on the game board
//
class BoardGrid : SKSpriteNode {
    
    let theme:Theme;
    
    // Grid textures
    let player1Texture:SKTexture;
    let player2Texture:SKTexture;
    
    let baseTexture:SKTexture;
    let removedTexture:SKTexture;
    let idelHilightTexture:SKTexture;
    let p1MoveHilightTexture:SKTexture;
    let p2MoveHilightTexture:SKTexture;
    
    // Grid Properties
    let gridWidth:CGFloat;
    let coordinate:GridCoordinate;

    // Run-time Status
    var gridStatus:GridStatus = GridStatus.idle;
    var isHilighted:Bool = false;
    
    
    // Constructor
    init(position:CGPoint, size:CGSize, coordinate:GridCoordinate, theme:Theme,
         player1IconIndex:Int, player2IconIndex:Int) {
        self.gridWidth = size.width;
        self.coordinate = coordinate;
        self.theme = theme;
        self.player1Texture = SKTexture(imageNamed: PlayerIconUtils.getIconName(player1IconIndex));
        self.player2Texture = SKTexture(imageNamed: PlayerIconUtils.getIconName(player2IconIndex));
        
        // Set up grid textures
        baseTexture = SKTexture(imageNamed: theme.baseTextureImageName);
        removedTexture = SKTexture(imageNamed: theme.removedTextureImageName);
        idelHilightTexture = SKTexture(imageNamed: theme.idelHilightTextureImageName);
        p1MoveHilightTexture = SKTexture(imageNamed: theme.p1MoveHilightTextureImageName);
        p2MoveHilightTexture = SKTexture(imageNamed: theme.p2MoveHilightTextureImageName);

        super.init(texture: baseTexture, color: SKColor.white, size: size);
        //self.path = CGPathCreateWithRoundedRect(CGRectMake(0, 0, gridWidth, gridWidth), gridWidth/16, gridWidth/16, nil);
        self.position = position;
        self.name = GameConstants.BOARDGRID_NODE_NAME;
        //self.strokeColor = SKColor.blackColor();
        self.zPosition = CGFloat(LayerDepth.gameboardgrid.rawValue);
        self.anchorPoint = CGPoint.zero;
        reset();
        
        //Add coordinate label on the test mode
        if (GameConfigUtils.isTestMode()) {
            let coordinateLabel:SKLabelNode = SKLabelNode();
            coordinateLabel.text = coordinate.toString();
            coordinateLabel.position = CGPoint(x: gridWidth/2, y: gridWidth/2);
            coordinateLabel.fontColor = SKColor.red;
            coordinateLabel.fontSize = 10;
            coordinateLabel.zPosition = CGFloat(LayerDepth.other.rawValue);
            self.addChild(coordinateLabel);
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        self.removeAllChildren();
        self.removeAllActions();
        print("BoardGrid \(coordinate.toString()) is deinited.")
    }
    
    // Update the grid status
    func update(_ newStatus: GridStatus) {
        // Only do update if status changed
        if (self.gridStatus == newStatus) {
            return;
        }
        
        // Update the grid based on the new status
        self.removeAllChildren();
        if (newStatus != GridStatus.idle) {
            // Added the object on the based layer for non-idle grid
            let addOnObject:SKSpriteNode = self.getAddOnObject(newStatus);
            self.addChild(addOnObject);
        }
        
        // Update the grid status
        //print("Grid \(coordinate.toString()) status changed: \(gridStatus) ==> \(newStatus)");
        self.gridStatus = newStatus;
    }
    
    // Reset the grid condition to the IDLE status
    func reset() {
        self.gridStatus = GridStatus.idle;
        self.removeAllChildren();
    }
    
    // Hilight the grid
    func hilight(_ targetStatus:GridStatus) {
        // Only allow to hilight the IDEL grid
        if (self.gridStatus == GridStatus.idle) {
            self.addChild(getAddOnObject(targetStatus, hilighted: true));
            isHilighted = true;
        }
    }
    
    // Remove the grid hilight
    func removeHilight() {
        // Only IDEL grid can be hilighted
        if (self.gridStatus == GridStatus.idle) {
            self.removeAllChildren();
            isHilighted = false;
        }
    }
    
    // Get the object added on to the base layer corresponding to the target status
    fileprivate func getAddOnObject(_ targetStatus:GridStatus, hilighted:Bool = false) -> SKSpriteNode {
        let addOnObject:SKSpriteNode = SKSpriteNode();
        if (targetStatus == GridStatus.removed) {
            if (hilighted) {
                addOnObject.texture = idelHilightTexture;
            } else {
                addOnObject.texture = removedTexture;
            }
        } else if (targetStatus == GridStatus.p1) {
            if (hilighted) {
                addOnObject.texture = p1MoveHilightTexture;
            } else {
                addOnObject.texture = player1Texture;
            }
        } else if (targetStatus == GridStatus.p2) {
            if (hilighted) {
                addOnObject.texture = p2MoveHilightTexture;
            } else {
                addOnObject.texture = player2Texture;
            }
        }
        addOnObject.size = CGSize(width: self.gridWidth, height: self.gridWidth);
        addOnObject.anchorPoint = CGPoint.zero;
        addOnObject.zPosition = CGFloat(LayerDepth.gridaddon.rawValue);
        if (hilighted) {
            addOnObject.alpha = 0.6;
        }
        return addOnObject;
    }
}
