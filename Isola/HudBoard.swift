//
//  HudBoard.swift
//  Isola
//
//  Created by Bolei Fu on 10/8/15.
//  Copyright © 2015 boleifu. All rights reserved.
//

import SpriteKit

//
// The HUD node that shows real-time game status
//
class HudBoard : SKSpriteNode {
    // HUD properties
    
    //let hudBoardTexture:SKTexture = SKTexture(imageNamed: "grey_panel");
    
    // HUD contents
    fileprivate var gameStatusLabel:SKLabelNode? = nil;
    fileprivate var versionLabel:SKLabelNode? = nil;
    
    let gameMsgLineNum = 5;
    fileprivate var gameMsgLabels:[SKLabelNode] = [];
    fileprivate var msgQueue = Queue<String>();
    fileprivate var msgRoleQueue = Queue<Role?>();

    
    
    // HUD properties
    fileprivate let gameStatusDuration:TimeInterval = 1;
    
    // Constructor
    init(size:CGSize) {
        super.init(texture: nil, color: SKColor.white, size: size);
        
        // Set up the game board
        self.name = GameConstants.HUDBOARD_NODE_NAME;
        self.texture?.filteringMode = SKTextureFilteringMode.nearest;
        self.color = SKColor.clear;

        
        setupHudBoard();
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        self.removeAllActions();
        self.removeAllChildren();
        print("HudBoard is deinited.")
    }
    
    // Update the game run-time status message
    func updateGameStatus(_ newStatusMsg:String) {
        // The message disappear after certain period
        let showGameStatusMsg:SKAction =
        SKAction.sequence(
            [SKAction.run({self.gameStatusLabel!.text = newStatusMsg;}),
                SKAction.wait(forDuration: gameStatusDuration),
                SKAction.run({self.gameStatusLabel!.text = GameConstants.NO_MESSAGE;})]);
        print(newStatusMsg);
        self.run(showGameStatusMsg);
    }
    
    // Add the game status message.
    // Once added, the message will appear on HUD.
    func addGameMsg(msg:String, role:Role?) {
        msgQueue.enqueue(msg)
        msgRoleQueue.enqueue(role)
        while (msgQueue.count > gameMsgLineNum) {
            _ = msgQueue.dequeue()
            _ = msgRoleQueue.dequeue()
        }
        refreshGameMsg()
    }
    
    func clearGameMsg() {
        msgQueue = Queue<String>()
        msgRoleQueue = Queue<Role?>()

        for msgLabel in gameMsgLabels {
            msgLabel.fontColor = GameConstants.BLACK_COLOR
            msgLabel.text = ""
        }
    }
    
    fileprivate func refreshGameMsg() {
        let msgNum = msgQueue.count
        
        if msgNum == 0 {
            clearGameMsg()
            return
        }
        
        for index in 0...msgNum-1 {
            let msg = msgQueue.get(index: msgNum-index-1)
            if msg == nil {
                gameMsgLabels[index].text = ""
            } else {
                gameMsgLabels[index].text = msg
            }
            
            let role = msgRoleQueue.get(index: msgNum-index-1)
            if role == nil {
                gameMsgLabels[index].fontColor = GameConstants.BLACK_COLOR
            } else if role! == Role.p1 {
                gameMsgLabels[index].fontColor = GameConstants.RED_COLOR
            } else if role! == Role.p2 {
                gameMsgLabels[index].fontColor = GameConstants.BLUE_COLOR
            }
        }
    }
    
    // Set up the HudBoard
    fileprivate func setupHudBoard() {
        // Setup Game Status label
        gameStatusLabel = SKLabelNode(fontNamed: "BanglaSangamMN-Bold ");
        gameStatusLabel!.text = "Game Status";
        gameStatusLabel!.fontSize = 15;
        gameStatusLabel!.fontColor = GameConstants.BLACK_COLOR;
        gameStatusLabel!.position = CGPoint(x: 0, y: -self.size.height/10);
        gameStatusLabel!.zPosition = CGFloat(LayerDepth.hudboard.rawValue);
        gameStatusLabel!.horizontalAlignmentMode = .center
        gameStatusLabel!.verticalAlignmentMode = .top
        self.addChild(gameStatusLabel!);
        
        // Setup game version label
        /*
        versionLabel = SKLabelNode(text: GameUtils.version());
        versionLabel!.fontName = "KenVector Future Thin Regular";
        versionLabel!.fontSize = 10;
        versionLabel!.fontColor = SKColor.blackColor();
        versionLabel!.position = CGPointMake(-self.size.width*3/8, self.size.height/5);
        versionLabel!.zPosition = CGFloat(LayerDepth.HUDBOARD.rawValue);
        self.addChild(versionLabel!);
        */

        let msgLabelLeftX = -self.size.width/20*9
        let msgLabelTopY = -self.size.height/4
        let msgLabelsHeight = self.size.height/4*3
        for index in 0...gameMsgLineNum-1 {
            let msgLabel = SKLabelNode(fontNamed: "ChalkboardSE-Regular")
            msgLabel.text = ""
            msgLabel.fontSize = 15
            msgLabel.fontColor = GameConstants.BLACK_COLOR
            msgLabel.horizontalAlignmentMode = .left
            msgLabel.verticalAlignmentMode = .top
            msgLabel.position = CGPoint(x: msgLabelLeftX, y: msgLabelTopY-msgLabelsHeight/CGFloat(gameMsgLineNum)*CGFloat(index))
            msgLabel.zPosition = CGFloat(LayerDepth.hudboard.rawValue)
            gameMsgLabels.append(msgLabel)
            self.addChild(msgLabel)
        }
    }
}
