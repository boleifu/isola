//
//  BoardHelper.swift
//  Isola
//
//  Created by Bolei Fu on 10/7/15.
//  Copyright © 2015 boleifu. All rights reserved.
//

//
// Some helper objects needed on the game board
//

// The coordinate of a grid on the board
struct GridCoordinate {
    var col:Int;    // x axis
    var row:Int;    // y axis
    
    func toString() -> String {
        return "[\(row),\(col)]";
    }
}

// The size of the game board defined in the grid number
struct BoardSize {
    var col:Int;
    var row:Int;
}

// The status of a grid
enum GridStatus {
    case idle;
    case removed;
    case p1;
    case p2;
    case unknown;
}
