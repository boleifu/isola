//
//  Theme.swift
//  Isola
//
//  Created by Bolei Fu on 10/17/15.
//  Copyright © 2015 boleifu. All rights reserved.
//

protocol Theme {
    var name:String {get};
    
    // BG image
    var topBackGroundImageName:String {get};
    var bottomBackGroundImageName:String {get};
    
    // Grid Image
    var baseTextureImageName:String {get};
    var removedTextureImageName:String {get};
    var idelHilightTextureImageName:String {get};
    var p1MoveHilightTextureImageName:String {get};
    var p2MoveHilightTextureImageName:String {get};
    
    // Game Sound
    var gameOverSoundName:String {get};
    var moveSoundName:String {get};
    var removeSoundName:String {get};
    var invalidMoveSoundName:String {get};
}
