//
//  CreditViewController.swift
//  Isola
//
//  Created by Bolei Fu on 10/18/15.
//  Copyright © 2015 boleifu. All rights reserved.
//

import UIKit
import MessageUI

class CreditViewController: UIViewController, MFMailComposeViewControllerDelegate {

    var backgroundImageView:UIImageView = UIImageView();
    var moveXOffset:CGFloat = 0;
    
    var iconImageViews:Array<UIImageView> = Array<UIImageView>();
    
    @IBOutlet weak var creditTitleLabel: UILabel!
    @IBOutlet weak var developedByLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var assetsCreditTextView: UITextView!
    
    
    // Animal Icons
    let iconRotateAngle:CGFloat = CGFloat(Double.pi/180*30);
    @IBOutlet weak var parrotIcon: UIImageView!
    @IBOutlet weak var pandaIcon: UIImageView!
    @IBOutlet weak var giraffeIcon: UIImageView!
    @IBOutlet weak var hippoIcon: UIImageView!
    @IBOutlet weak var pigIcon: UIImageView!
    @IBOutlet weak var penguinIcon: UIImageView!
    
    @IBOutlet weak var emailMeButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    
    deinit {
        NotificationCenter.default.removeObserver(self);
        print("CreditViewController is deinited.");
    }
    
    override func viewDidLoad() {
        super.viewDidLoad();
        // Start animation when comes to the forground
        NotificationCenter.default.addObserver(self, selector: #selector(CreditViewController.enterForeground(_:)), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil);
        
        // Background color
        //self.view.backgroundColor = UIColor(red: 0.298, green:0.851, blue:0.392, alpha:1);
        
        // Background image view
        backgroundImageView = UIImageView();
        backgroundImageView.contentMode = UIViewContentMode.scaleAspectFill;
        backgroundImageView.layer.zPosition = -1;
        self.view.addSubview(backgroundImageView);
        
        // Labels
        creditTitleLabel.font = UIFont(name: "Kenvector Future", size: 20);
        developedByLabel.font = UIFont(name: "Kenvector Future Thin", size: 20);
        developedByLabel.textColor = UIColor.gray;
        authorLabel.font = UIFont(name: "Kenvector Future Thin", size: 25);
        //assetsCreditTextView.font = UIFont(name: "Kenvector Future", size: 10);
        assetsCreditTextView.textColor = UIColor.gray;

        // Button
        emailMeButton.titleLabel?.font = UIFont(name: "Kenvector Future", size: 20);
        //emailMeButton.setTitle(GameConstants.AUTHOR_EMAIL, forState: UIControlState.Highlighted);
        backButton.titleLabel?.font = UIFont(name: "Kenvector Future", size: 20);
        
    }
    
    override func viewWillAppear(_ animated:Bool) {
        super.viewWillAppear(animated);
        
        startAnimation();
    }
    
    @IBAction func clickedEmailMeButton(_ sender: UIButton) {
        AudioUtils.playButtonClickSound();
        // Send email
        let mailComposerVC = MFMailComposeViewController();
        mailComposerVC.mailComposeDelegate = self;
        mailComposerVC.setToRecipients([GameConstants.AUTHOR_EMAIL]);
        if (MFMailComposeViewController.canSendMail()) {
            self.present(mailComposerVC, animated: true, completion: nil);
        } else {
            print("Error: Cannot send email.");
        }
    }
    
    @IBAction func clickedBackButton(_ sender: UIButton) {
        AudioUtils.playButtonClickSound();
        self.dismiss(animated: true, completion: nil);
    }
    
    @objc fileprivate func enterForeground(_ notification: Notification) {
        startAnimation();
    }
    
    fileprivate func startAnimation() {
        // Init animated object satus
        initBackgroundImage();
        // Animal Icons
        self.parrotIcon.transform = CGAffineTransform(rotationAngle: -iconRotateAngle);
        self.pandaIcon.transform = CGAffineTransform(rotationAngle: -iconRotateAngle);
        self.giraffeIcon.transform = CGAffineTransform(rotationAngle: -iconRotateAngle);
        self.hippoIcon.transform = CGAffineTransform(rotationAngle: iconRotateAngle);
        self.pigIcon.transform = CGAffineTransform(rotationAngle: iconRotateAngle);
        self.penguinIcon.transform = CGAffineTransform(rotationAngle: iconRotateAngle);
        
        
        // Move the bg image horizontally
        UIView.animate(withDuration: 10, delay: 0, options: [UIViewAnimationOptions.repeat, UIViewAnimationOptions.autoreverse] , animations: {
            self.backgroundImageView.frame = CGRect(
                origin: CGPoint(x: -self.moveXOffset, y: 0),
                size: self.backgroundImageView.frame.size);
            },
            completion: nil);
        
        // Make the animal icon rotate
        UIView.animate(withDuration: 1, delay: 0, options: [UIViewAnimationOptions.repeat, UIViewAnimationOptions.autoreverse] , animations: {
            self.parrotIcon.transform = CGAffineTransform(rotationAngle: self.iconRotateAngle);
            self.pandaIcon.transform = CGAffineTransform(rotationAngle: self.iconRotateAngle);
            self.giraffeIcon.transform = CGAffineTransform(rotationAngle: self.iconRotateAngle);
            self.hippoIcon.transform = CGAffineTransform(rotationAngle: -self.iconRotateAngle);
            self.pigIcon.transform = CGAffineTransform(rotationAngle: -self.iconRotateAngle);
            self.penguinIcon.transform = CGAffineTransform(rotationAngle: -self.iconRotateAngle);
            },
            completion: nil);
    }
    
    fileprivate func initBackgroundImage() {
        // Background image
        let backgroundImage:UIImage = GameUtils.getRandomBackgroundUncoloredImage()!;
        
        // Scale image
        let scaledImage:UIImage = GameUtils.scaleImageToFitHeight(backgroundImage, targetHeight: self.view.bounds.size.height);
        backgroundImageView.image = scaledImage;
        moveXOffset = abs(scaledImage.size.width-self.view.frame.size.width)/2;
        backgroundImageView.frame = CGRect(origin: CGPoint(x: moveXOffset, y: 0), size: self.view.frame.size);
    }
    
    // MARK: MFMailComposeViewControllerDelegate Method
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}
