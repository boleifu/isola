//
//  GamePacketUtils.swift
//  Zoo Battle
//
//  Created by Bolei Fu on 9/7/17.
//  Copyright © 2017 boleifu. All rights reserved.
//

import Foundation

struct PlayerInfoPacket {
    var playrIconIndex:Int
    
    var playerScore:Int
    var playerWinNum:Int
    var playerLostNum:Int
    
    // Game proposal
    var rollNum:UInt32
    var boardWidth:UInt32
    var gameThemeIndex:UInt32
}

struct PlayerTurnPacket {
    var turnNum:Int
    var col:Int;    // x axis
    var row:Int;    // y axis
}

class PacketHelper {
    static func encodeStringPacket(packet: String) -> Data {
        return packet.data(using: String.Encoding.utf8)!
    }
    
    static func decodeStringPacket(data: Data) -> String? {
        return String(data: data, encoding: String.Encoding.utf8)
    }
    
    static func encodePlayerInfoPacket(packet: PlayerInfoPacket) -> Data {
        var packet = packet
        return Data(bytes: &packet, count: MemoryLayout<PlayerInfoPacket>.size)
    }
    
    static func decodePlayerInfoPacket(data: Data) -> PlayerInfoPacket? {
        var tempBuffer:PlayerInfoPacket? = nil
        data.withUnsafeBytes({(bytes: UnsafePointer<PlayerInfoPacket>)->Void in
            tempBuffer = UnsafePointer<PlayerInfoPacket>(bytes).pointee
        })
        return tempBuffer
    }
    
    static func encodePlayerTurnPacket(packet: PlayerTurnPacket) -> Data {
        var packet = packet
        return Data(bytes: &packet, count: MemoryLayout<PlayerTurnPacket>.size)
    }
    
    static func decodePlayerTurnPacket(data: Data) -> PlayerTurnPacket? {
        var tempBuffer:PlayerTurnPacket? = nil
        data.withUnsafeBytes({(bytes: UnsafePointer<PlayerTurnPacket>)->Void in
            tempBuffer = UnsafePointer<PlayerTurnPacket>(bytes).pointee
        })
        return tempBuffer
    }
}
