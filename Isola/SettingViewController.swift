//
//  SettingViewController.swift
//  Isola
//
//  Created by Bolei Fu on 10/17/15.
//  Copyright © 2015 boleifu. All rights reserved.
//

import UIKit

class SettingViewController: UIViewController{
    
    var backgroundImageView:UIImageView = UIImageView();
    var moveXOffset:CGFloat = 0;
    
    @IBOutlet weak var settingTitleLabel: UILabel!
    
    @IBOutlet weak var settingClickSoundLabel: UILabel!
    @IBOutlet weak var settingClickSoundEnableButton: UIButton!
    @IBOutlet weak var settingClickSoundDisableButton: UIButton!
    
    @IBOutlet weak var settingSFXLabel: UILabel!
    @IBOutlet weak var settingSFXEnableButton: UIButton!
    @IBOutlet weak var settingSFXDisableButton: UIButton!
    
    @IBOutlet weak var settingBGMLabel: UILabel!
    @IBOutlet weak var settingBGMEnableButton: UIButton!
    @IBOutlet weak var settingBGMDisableButton: UIButton!

    @IBOutlet weak var settingMoveHintLabel: UILabel!
    @IBOutlet weak var settingMoveHintEnableButton: UIButton!
    @IBOutlet weak var settingMoveHintDisableButton: UIButton!
    
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    // Game configurations
    var clickSoundEnabled:Bool = false;
    var sfxEnabled:Bool = false;
    var bgmEnabled:Bool = false;
    var moveHintEnabled:Bool = false;
    var p1IconIndex:Int = 0;
    var p2IconIndex:Int = 0;
    
    deinit {
        NotificationCenter.default.removeObserver(self);
        print("SettingViewController is deinited.");
    }
    
    override func viewDidLoad() {
        super.viewDidLoad();
        // Start animation when comes to the forground
        NotificationCenter.default.addObserver(self, selector: #selector(SettingViewController.enterForeground(_:)), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil);
        
        // Background color
        //self.view.backgroundColor = UIColor(red: 0.298, green:0.851, blue:0.392, alpha:1);
        
        // Background image view
        backgroundImageView = UIImageView();
        backgroundImageView.contentMode = UIViewContentMode.scaleAspectFill;
        backgroundImageView.layer.zPosition = -1;
        self.view.addSubview(backgroundImageView);
        
        // Title label
        settingTitleLabel.font = UIFont(name: "Kenvector Future", size: 20);
        
        // Click Sound setting
        settingClickSoundLabel.font = UIFont(name: "Kenvector Future Thin", size: 15);

        
        // SFX setting
        settingSFXLabel.font = UIFont(name: "Kenvector Future Thin", size: 15);
        
        // BGM setting
        settingBGMLabel.font = UIFont(name: "Kenvector Future Thin", size: 15);
        
        // Move Hint setting
        settingMoveHintLabel.font = UIFont(name: "Kenvector Future Thin", size: 15);
        
        // Save button
        saveButton.titleLabel?.font = UIFont(name: "Kenvector Future", size: 20);
        saveButton.setBackgroundImage(UIImage(named: "green_button00"), for: UIControlState())
        saveButton.setBackgroundImage(UIImage(named: "green_button01"), for: UIControlState.highlighted)
        
        // Cancel button
        cancelButton.titleLabel?.font = UIFont(name: "Kenvector Future", size: 20);
        cancelButton.setBackgroundImage(UIImage(named: "red_button01"), for: UIControlState())
        cancelButton.setBackgroundImage(UIImage(named: "red_button02"), for: UIControlState.highlighted)
        
        // Load current game configuration
        loadCurrenConfig();
    }
    
    override func viewWillAppear(_ animated:Bool) {
        super.viewWillAppear(animated);
        
        startAnimation();
    }
    
    // Button action handler
    @IBAction func clickedSaveButton(_ sender: UIButton) {
        AudioUtils.playButtonClickSound();
        // Update volumn
        AudioUtils.updateSEVolumn(enabled: clickSoundEnabled)
        AudioUtils.updateBGMVolumn(enabled: bgmEnabled)
        
        // Persist the update
        GameConfigUtils.updateGameConfig(clickSoundEnabled:clickSoundEnabled, gameSFXEnabled:sfxEnabled, gameBGMEnabled:bgmEnabled, moveHintEnabled:moveHintEnabled);
        self.dismiss(animated: true, completion: nil);
    }

    @IBAction func clickedCancelButton(_ sender: UIButton) {
        AudioUtils.playButtonClickSound();
        self.dismiss(animated: true, completion: nil);
    }
    
    @IBAction func clickedClickSoundEnableButton(_ sender: UIButton) {
        AudioUtils.playButtonClickSound();
        setClickSoundEnabled(true);
    }
    
    @IBAction func clickedClickSoundDisableButton(_ sender: UIButton) {
        AudioUtils.playButtonClickSound();
        setClickSoundEnabled(false);
    }
    
    @IBAction func clickedSFXEnableButton(_ sender: UIButton) {
        AudioUtils.playButtonClickSound();
        setSfxEnabled(true);
    }
    
    @IBAction func clickedSFXDisableButton(_ sender: UIButton) {
        AudioUtils.playButtonClickSound();
        setSfxEnabled(false);
    }
    
    @IBAction func clickBGMEnableButton(_ sender: UIButton) {
        AudioUtils.playButtonClickSound();
        setBGMEnabled(true)
    }
    
    @IBAction func clickBGMDisableButton(_ sender: UIButton) {
        AudioUtils.playButtonClickSound();
        setBGMEnabled(false)
    }
    
    @IBAction func clickedMoveHintEnableButton(_ sender: UIButton) {
        AudioUtils.playButtonClickSound();
        setMoveHintEnabled(true);
    }
    
    @IBAction func clickedMoveHintDisableButton(_ sender: UIButton) {
        AudioUtils.playButtonClickSound();
        setMoveHintEnabled(false);
    }
    
    // Game configuration handler
    
    
    // Show the current configuration setting on the screen
    fileprivate func loadCurrenConfig() {        
        // Load Click sound settings
        clickSoundEnabled = GameConfigUtils.isClickSoundEnabled();
        setClickSoundEnabled(clickSoundEnabled);
        
        // Load SFX setting
        sfxEnabled = GameConfigUtils.isSFXEnabled();
        setSfxEnabled(sfxEnabled);
        
        // Load BGM setting
        bgmEnabled = GameConfigUtils.isBGMEnabled();
        setBGMEnabled(bgmEnabled)
        
        // Load Move Hint setting
        moveHintEnabled = GameConfigUtils.isMoveHintEnabled();
        setMoveHintEnabled(moveHintEnabled);
    }
    
    fileprivate func setClickSoundEnabled(_ enabled:Bool) {
        settingClickSoundEnableButton.isSelected = enabled;
        settingClickSoundDisableButton.isSelected = !enabled;
        clickSoundEnabled = enabled;
    }
    
    fileprivate func setSfxEnabled(_ enabled:Bool) {
        settingSFXEnableButton.isSelected = enabled;
        settingSFXDisableButton.isSelected = !enabled;
        sfxEnabled = enabled;
    }

    fileprivate func setBGMEnabled(_ enabled:Bool) {
        settingBGMEnableButton.isSelected = enabled;
        settingBGMDisableButton.isSelected = !enabled;
        bgmEnabled = enabled;
    }
    
    fileprivate func setMoveHintEnabled(_ enabled:Bool) {
        settingMoveHintEnableButton.isSelected = enabled;
        settingMoveHintDisableButton.isSelected = !enabled;
        moveHintEnabled = enabled;
    }
    
    @objc fileprivate func enterForeground(_ notification: Notification) {
        startAnimation();
    }
    
    fileprivate func startAnimation() {
        // Init animated object satus
        initBackgroundImage();
        
        // Move the bg image horizontally
        UIView.animate(withDuration: 10, delay: 0, options: [UIViewAnimationOptions.repeat, UIViewAnimationOptions.autoreverse] , animations: {
            self.backgroundImageView.frame = CGRect(
                origin: CGPoint(x: -self.moveXOffset, y: 0),
                size: self.backgroundImageView.frame.size);
            },
            completion: nil);
    }
    
    fileprivate func initBackgroundImage() {
        // Background image
        let backgroundImage:UIImage = GameUtils.getRandomBackgroundUncoloredImage()!;
        
        // Scale image
        let scaledImage:UIImage = GameUtils.scaleImageToFitHeight(backgroundImage, targetHeight: self.view.bounds.size.height);
        backgroundImageView.image = scaledImage;
        moveXOffset = abs(scaledImage.size.width-self.view.frame.size.width)/2;
        backgroundImageView.frame = CGRect(origin: CGPoint(x: moveXOffset, y: 0), size: self.view.frame.size);
    }
}
