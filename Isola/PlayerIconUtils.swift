//
//  PlayerIconUtils.swift
//  Isola
//
//  Created by Bolei Fu on 10/17/15.
//  Copyright © 2015 boleifu. All rights reserved.
//

import Foundation
import UIKit

class PlayerIconUtils {
    static let playerIconArray:Array<String> = ["elephant", "giraffe", "hippo", "monkey", "panda", "parrot", "penguin", "pig", "rabbit", "snake"];
    
    static let playerRoundIconArray:Array<String> = ["elephant_round", "giraffe_round", "hippo_round", "monkey_round", "panda_round", "parrot_round", "penguin_round", "pig_round", "rabbit_round", "snake_round"];
    
    static func getImageOfIcon(_ iconIndex:Int) -> UIImage? {
        let iconName:String = PlayerIconUtils.getIconName(iconIndex);
        if (PlayerIconUtils.playerIconArray.contains(iconName)) {
            return UIImage(named: iconName);
        } else {
            return nil;
        }
    }
    
    static func getIconName(_ index: Int) -> String {
        if (index < 0 || index >= PlayerIconUtils.playerIconArray.count) {
            print("Error: Invalid icon index \(index)");
            return PlayerIconUtils.playerIconArray[0];
        }
        
        return PlayerIconUtils.playerIconArray[index];
    }
    
    static func getNextIconIndex(_ currentIndex:Int) -> Int {
        if (currentIndex < 0 || currentIndex >= PlayerIconUtils.playerIconArray.count-1) {
            return 0;
        }
        return currentIndex+1;
    }
    
    static func getPrevIconIndex(_ currentIndex:Int) -> Int {
        if (currentIndex <= 0 || currentIndex > PlayerIconUtils.playerIconArray.count-1) {
            return PlayerIconUtils.playerIconArray.count-1;
        }
        return currentIndex-1;
    }
    
    static func getRandomRoundIcon() -> UIImage? {
        let randInt:UInt32 = PlayerIconUtils.getRandomIconIndex();
        return UIImage(named: PlayerIconUtils.playerRoundIconArray[Int(randInt)]);
    }
    
    static func getRandomIconIndex() -> UInt32 {
        let randInt:UInt32 = arc4random_uniform(UInt32(PlayerIconUtils.playerRoundIconArray.count));
        return randInt
    }
    
}
