//
//  AudioUtils.swift
//  Zoo Battle
//
//  Created by Bolei Fu on 12/2/16.
//  Copyright © 2016 boleifu. All rights reserved.
//

import AVFoundation

class AudioUtils {
    
    static var seVolumn:Float = GameConstants.DEFAULT_SE_VOLUMN;
    static var bgmVolumn:Float = GameConstants.DEFAULT_BGM_VOLUMN;
    
    static var mainBgmPlayer : AVAudioPlayer? = nil;
    static var gameBgmPlayer : AVAudioPlayer? = nil;
    
    static var buttonClickSoundPlayer : AVAudioPlayer? = nil;
    static var gameOverSoundPlayer : AVAudioPlayer? = nil;
    
    static func createAudioPlayer(_ audioFileName:String, _ audioFileType:String) -> AVAudioPlayer? {
        // Setup sound player
        let path = Bundle.main.path(forResource: "Sounds/"+audioFileName, ofType: audioFileType);
        if (path == nil) {
            print("ERROR: Unable to find the audio file \(audioFileName).\(audioFileType)");
            return nil;
        }
        let url = URL(fileURLWithPath: path!);
        do {
            return try AVAudioPlayer(contentsOf: url);
        } catch let error as NSError {
            print("ERROR: Failed to create the audio player for the file \(audioFileName).\(audioFileType). Error: \(error.localizedDescription)");
            return nil;
        }
    }
    
    
    static func setupAllAudioPlayers() {
        // Load volumn setting
        if (!GameConfigUtils.isClickSoundEnabled()) {
            seVolumn = 0;
        }

        if (!GameConfigUtils.isBGMEnabled()) {
            bgmVolumn = 0;
        }
        
        // Sound effects
        if (buttonClickSoundPlayer == nil) {
            setupButtonClickSoundPlayer();
        }
        
        // BGM
        if (mainBgmPlayer == nil) {
            setupMainBgmPlayer();
        }
        
        if (gameBgmPlayer == nil) {
            setupGameBgmPlayer();
        }
    }
    
    
    // Button click sound
    static func setupButtonClickSoundPlayer() {
        buttonClickSoundPlayer = createAudioPlayer(GameConstants.BUTTON_CLICK_SOUND_FILE_NAME, GameConstants.WAV_FILE_TYPE);
    }
    
    static func playButtonClickSound() {
        if (buttonClickSoundPlayer == nil) {
            setupButtonClickSoundPlayer();
        }
        // Double check it
        if (buttonClickSoundPlayer != nil) {
            DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async {
                buttonClickSoundPlayer?.volume = seVolumn;
                buttonClickSoundPlayer?.prepareToPlay();
                buttonClickSoundPlayer?.play();
            }
        }
    }
    
    // Game Over sound
    static func setupGameOverSoundPlayer() {
        gameOverSoundPlayer = createAudioPlayer("jingles_SAX12", GameConstants.WAV_FILE_TYPE)
    }
    
    static func playGameOverSound() {
        if (gameOverSoundPlayer == nil) {
            setupGameOverSoundPlayer();
        }
        // Double check it
        if (gameOverSoundPlayer != nil) {
            DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async {
                gameOverSoundPlayer?.volume = seVolumn;
                gameOverSoundPlayer?.prepareToPlay();
                gameOverSoundPlayer?.play();
            }
        }
    }
    
    // Main BGM sound
    static func setupMainBgmPlayer() {
        mainBgmPlayer = createAudioPlayer(GameConstants.MAIN_THEME_BGM_FILE_NAME, GameConstants.MP3_FILE_TYPE);
    }

    static func playMainBgm() {
        if (mainBgmPlayer == nil) {
            setupMainBgmPlayer();
        }
        // Double check it
        if (mainBgmPlayer != nil) {
            DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async {
                mainBgmPlayer?.numberOfLoops = -1;
                mainBgmPlayer?.currentTime = 0;
                mainBgmPlayer?.volume = bgmVolumn;
                mainBgmPlayer?.prepareToPlay();
                mainBgmPlayer?.play();
            }
        }
    }
    
    static func stopMainBgm() {
        if (mainBgmPlayer == nil) {
            return;
        }
        mainBgmPlayer?.stop();
    }
    
    // Game BGM sound
    static func setupGameBgmPlayer() {
        gameBgmPlayer = createAudioPlayer(GameConstants.GAME_BGM_FILE_NAME, GameConstants.MP3_FILE_TYPE);
    }
    
    static func playGameBgm() {
        if (gameBgmPlayer == nil) {
            setupGameBgmPlayer();
        }
        // Double check it
        if (gameBgmPlayer != nil) {
            DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async {
                gameBgmPlayer?.numberOfLoops = -1;
                gameBgmPlayer?.currentTime = 0;
                gameBgmPlayer?.volume = bgmVolumn;
                gameBgmPlayer?.prepareToPlay();
                gameBgmPlayer?.play();
            }
        }
    }
    
    // Audio control
    static func stopGameBgm() {
        if (gameBgmPlayer == nil) {
            return;
        }
        gameBgmPlayer?.stop();
    }
    
    static func updateBGMVolumn(enabled:Bool) {
        var vol = GameConstants.DEFAULT_BGM_VOLUMN;
        if (!enabled) {
           vol = 0
        }
        
        AudioUtils.bgmVolumn = vol
        if (mainBgmPlayer != nil) {
            mainBgmPlayer?.volume = vol
        }
        if (gameBgmPlayer != nil) {
            gameBgmPlayer?.volume = vol
        }
    }
    
    static func updateSEVolumn(enabled:Bool) {
        var vol = GameConstants.DEFAULT_SE_VOLUMN
        if (!enabled) {
            vol = 0
        }
        
        AudioUtils.seVolumn = vol
        if (buttonClickSoundPlayer != nil) {
            buttonClickSoundPlayer?.volume = vol
        }
    }
}
