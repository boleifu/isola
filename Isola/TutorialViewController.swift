//
//  TutorialViewController.swift
//  Isola
//
//  Created by Bolei Fu on 10/23/15.
//  Copyright © 2015 boleifu. All rights reserved.
//

import UIKit

class TutorialViewController: UIViewController {

    @IBOutlet weak var howToPlayLabel: UILabel!
    @IBOutlet weak var rightButton: UIButton!
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var contentImage: UIImageView!
    
    var backgroundImageView:UIImageView = UIImageView();
    var moveXOffset:CGFloat = 0;
    
    let stepImageNames:Array<String> = ["Step1", "Step2", "Step3", "Step4"];
    var imageHeight:CGFloat = 0;
    var currentIndex:Int = 0;
    
    deinit {
        NotificationCenter.default.removeObserver(self);
        print("TutorialViewController is deinited.");
    }
    
    override func viewDidLoad() {
        super.viewDidLoad();
        // Start animation when comes to the forground
        NotificationCenter.default.addObserver(self, selector: #selector(TutorialViewController.enterForeground(_:)), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil);
        
        // Background color
        //self.view.backgroundColor = UIColor(red: 0.298, green:0.851, blue:0.392, alpha:1);
        
        // Background image view
        backgroundImageView = UIImageView();
        backgroundImageView.contentMode = UIViewContentMode.scaleAspectFill;
        backgroundImageView.layer.zPosition = -1;
        self.view.addSubview(backgroundImageView);
        
        // View title label
        howToPlayLabel.font = UIFont(name: "Kenvector Future", size: 20);
        
        // Image content view
        imageHeight = self.view.frame.size.height/2;
        showStepImage(currentIndex);
        
        // Back button
        backButton.titleLabel?.font = UIFont(name: "Kenvector Future", size: 20);
        backButton.setBackgroundImage(UIImage(named: "red_button01"), for: UIControlState())
        backButton.setBackgroundImage(UIImage(named: "red_button02"), for: UIControlState.highlighted)
    }
    
    override func viewWillAppear(_ animated:Bool) {
        super.viewWillAppear(animated);
        
        startAnimation();
    }
    
    fileprivate func showStepImage(_ index:Int) {
        let stepImage:UIImage = UIImage(named: stepImageNames[index])!;
        //let scaledImage:UIImage = GameUtils.scaleImageToFitHeight(stepImage, targetHeight: imageHeight);
        //contentImage.frame.size = scaledImage.size;
        contentImage.image = stepImage;
        
        leftButton.isHidden = currentIndex <= 0;
        rightButton.isHidden = currentIndex >= stepImageNames.count-1;
    }
    
    @IBAction func leftButtonClicked(_ sender: UIButton) {
        AudioUtils.playButtonClickSound();
        currentIndex -= 1;
        showStepImage(currentIndex);
    }
    
    @IBAction func rightButtonClicked(_ sender: UIButton) {
        AudioUtils.playButtonClickSound();
        currentIndex += 1;
        showStepImage(currentIndex);
    }
    
    @IBAction func backButtonClicked(_ sender: UIButton) {
        AudioUtils.playButtonClickSound();
        self.dismiss(animated: true, completion: nil);
    }
    
    @objc fileprivate func enterForeground(_ notification: Notification) {
        startAnimation();
    }
    
    fileprivate func startAnimation() {
        // Init animated object satus
        initBackgroundImage();
        
        // Move the bg image horizontally
        UIView.animate(withDuration: 10, delay: 0, options: [UIViewAnimationOptions.repeat, UIViewAnimationOptions.autoreverse] , animations: {
            self.backgroundImageView.frame = CGRect(
                origin: CGPoint(x: -self.moveXOffset, y: 0),
                size: self.backgroundImageView.frame.size);
            },
            completion: nil);
    }
    
    fileprivate func initBackgroundImage() {
        // Background image
        let backgroundImage:UIImage = GameUtils.getRandomBackgroundUncoloredImage()!;
        
        // Scale image
        let scaledImage:UIImage = GameUtils.scaleImageToFitHeight(backgroundImage, targetHeight: self.view.bounds.size.height);
        backgroundImageView.image = scaledImage;
        moveXOffset = abs(scaledImage.size.width-self.view.frame.size.width)/2;
        backgroundImageView.frame = CGRect(origin: CGPoint(x: moveXOffset, y: 0), size: self.view.frame.size);
    }
    
}
