//
//  GameBoard.swift
//  Isola
//
//  Created by Bolei Fu on 10/6/15.
//  Copyright © 2015 boleifu. All rights reserved.
//

import SpriteKit

//
// The GameBoard node
//
class GameBoard : SKSpriteNode {
    let theme:Theme;
    let player1IconIndex:Int
    let player2IconIndex:Int
    
    // Board status
    let boardSize:BoardSize;
    var gridSize:CGSize = CGSize.zero;
    
    // Constructor
    init(size:CGSize, boardSize:BoardSize, gridNum:Int, theme:Theme,
         player1IconIndex:Int, player2IconIndex:Int) {
        self.boardSize = boardSize;
        self.theme = theme;
        self.player1IconIndex = player1IconIndex
        self.player2IconIndex = player2IconIndex
        super.init(texture: nil, color: SKColor.white, size: size);
        
        // Set up the game board
        self.name = GameConstants.GAMEBOARD_NODE_NAME;
        gridSize = setupGameBoard(gridNum);
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        self.removeAllActions();
        self.removeAllChildren();
        print("GameBoard is deinited.")
    }
    
    // Process a move, and updating the grid related to the move.
    func procssMove(_ targetGrid:GridCoordinate, desiredStatus:GridStatus) {
        // Get the target grid
        let grid:BoardGrid? = getGrid(targetGrid);
        
        // Process the move if the grid exists
        if (grid != nil) {
            // Player Move, clean up the old player position
            if (desiredStatus == GridStatus.p1 || desiredStatus == GridStatus.p2) {
                let playerGrids:Array<BoardGrid> = getGridWithStatus(desiredStatus);
                for grid:BoardGrid in playerGrids {
                    grid.update(GridStatus.idle);
                }
            }
            
            // Update the target grid to the desired status
            grid!.update(desiredStatus);
        } else {
            print("Error: Cannot find grid \(targetGrid.toString()).")
        }
    }
    
    // Check whether the given move is valid to process
    // Verify a targetGrid can be set to the desired status
    func isMoveValid(_ targetGrid:GridCoordinate, desiredStatus:GridStatus) -> Bool{
        // Get the target grid
        let grid:BoardGrid? = getGrid(targetGrid);
        
        if (grid != nil) {
            let existingGridStatus:GridStatus = grid!.gridStatus;
            
            if (desiredStatus == GridStatus.idle || desiredStatus == GridStatus.unknown) {
                // Player cannot set Grid to IDLE or UNKNOWN condition
                return false;
            } else if (desiredStatus == GridStatus.removed) {
                // Can only remove IDLE grid
                return existingGridStatus == GridStatus.idle
            } else if (desiredStatus == GridStatus.p1 || desiredStatus == GridStatus.p2) {
                // Player can only move to the IDLE grid
                if (existingGridStatus != GridStatus.idle) {
                    return false;
                }
                
                // Player can only move to an adjacent grid
                let adjacentGrids:Array<BoardGrid> = getAdjacentGrids(grid!.coordinate);
                for adjacentGrid:BoardGrid in adjacentGrids {
                    if (adjacentGrid.gridStatus == desiredStatus) {
                        return true;
                    }
                }
                
                // Otherwise not valid
                return false;
            } else {
                // All other conditions are invalid
                return false;
            }
        } else {
            // Cannot find the grid, move not valid
            print("Error: Cannot find grid \(targetGrid.toString()).")
            return false;
        }
    }
    
    // Check whether the given position on the screen is in the board
    func isInTheBoard(_ posOnScreen:CGPoint) -> Bool {
        let lowerLeft:CGPoint = CGPoint.zero;
        let upRight:CGPoint = CGPoint(x: self.size.width, y: self.size.height);
        
        return posOnScreen.x > lowerLeft.x
            && posOnScreen.y > lowerLeft.y
            && posOnScreen.x < upRight.x
            && posOnScreen.y < upRight.y;
    }
    
    // Hilight the available move for the desired status
    func hilightAvailableMove(_ desiredStatue: GridStatus) {
        for child:SKNode in self.children {
            if (child.name == GameConstants.BOARDGRID_NODE_NAME) {
                let boardGrid:BoardGrid = child as! BoardGrid;
                // Can only do the move on the IDLE node
                if (boardGrid.gridStatus == GridStatus.idle) {
                    if (isMoveValid(boardGrid.coordinate, desiredStatus: desiredStatue)) {
                        boardGrid.hilight(desiredStatue);
                    }
                }
            }
        }
    }
    
    // Remove all hilighted move
    func removeHilightedMove() {
        for child:SKNode in self.children {
            if (child.name == GameConstants.BOARDGRID_NODE_NAME) {
                let boardGrid:BoardGrid = child as! BoardGrid;
                if (boardGrid.isHilighted) {
                    boardGrid.removeHilight();
                }
            }
        }
    }
    
    // Get the status of each grid on the GameBoard
    func getBoardStat() -> GameBoardState {
        // Get Player1 Grid
        let player1Grid:BoardGrid? = getGridWithStatus(GridStatus.p1).first;
        var player1GridCoord = GameConstants.INVALID_COORDINATE;
        if (player1Grid != nil) {
            player1GridCoord = player1Grid!.coordinate;
        }
        
        // Get Player2 Grid
        let player2Grid:BoardGrid? = getGridWithStatus(GridStatus.p2).first;
        var player2GridCoord = GameConstants.INVALID_COORDINATE;
        if (player2Grid != nil) {
            player2GridCoord = player2Grid!.coordinate;
        }
        
        // Get Board State
        // Init board stat
        var boardState:Array<Array<GridStatus>> =
        Array<Array<GridStatus>>(repeating: Array<GridStatus>(repeating: GridStatus.unknown, count: boardSize.col),
            count: boardSize.row);
        
        // Go through all grid and save the grid
        for child:SKNode in self.children {
            if (child.name == GameConstants.BOARDGRID_NODE_NAME) {
                let boardGrid:BoardGrid = child as! BoardGrid;
                boardState[boardGrid.coordinate.row][boardGrid.coordinate.col] = boardGrid.gridStatus;
            }
        }
        
        //print("\(boardState)");
        return GameBoardState(player1Grid: player1GridCoord, player2Grid: player2GridCoord, boardSize: boardSize, boardState: boardState);
    }
    
    // Get all BoardGrids with the given status
    func getGridWithStatus(_ status:GridStatus) -> Array<BoardGrid>{
        var targetGrids:Array<BoardGrid> = Array<BoardGrid>();
        for child:SKNode in self.children {
            if (child.name == GameConstants.BOARDGRID_NODE_NAME) {
                let boardGrid:BoardGrid = child as! BoardGrid;
                if (boardGrid.gridStatus == status) {
                    targetGrids.append(boardGrid);
                }
            }
        }
        return targetGrids;
    }
    
    // Set up all grids on the game board, and init their status
    fileprivate func setupGameBoard(_ gridNum:Int) -> CGSize {
        // gridNum can only be positive
        if (gridNum <= 0) {
            print("Error: Invalid GameBoard \(gridNum)x\(gridNum)");
            return CGSize.zero;
        }
        
        print("Setting up GameBoard \(gridNum)x\(gridNum) ...");
        
        // Get grid properties
        let gridWidth:CGFloat = CGFloat(Int(self.size.width/CGFloat(gridNum)));
        let gridSize:CGSize = CGSize(width: gridWidth, height: gridWidth);
        print("Got grid size = \(gridSize)");
        
        // Set up all grids
        for x in 0 ..< gridNum {
            for y in 0 ..< gridNum {
                let gridPos:CGPoint = BoardUtils.getGridPosition(x, y, gridWidth:gridWidth);
                let grid:BoardGrid = BoardGrid(position: gridPos, size: gridSize, coordinate:GridCoordinate(col: x, row: y), theme: theme, player1IconIndex: player1IconIndex, player2IconIndex: player2IconIndex);
                self.addChild(grid);
                print("BoardGrid [\(x),\(y)] set. Pos = \(gridPos)");
            }
        }

        // Set up Player grids
        resetPlayerPosition();

        print("Finishing setting up GameBoard \(gridNum)x\(gridNum)");
        return gridSize;
    }
    
    func resetGameBoard() {
        for child:SKNode in self.children {
            if (child.name == GameConstants.BOARDGRID_NODE_NAME) {
                let boardGrid:BoardGrid = child as! BoardGrid;
                boardGrid.reset();
            }
        }
        
        resetPlayerPosition();
    }
    
    // Reset the position of each player
    fileprivate func resetPlayerPosition() {
        let playerCol:Int = Int(boardSize.col/2);
        print("Adding Players 1 to the board ...");
        getGrid(GridCoordinate(col: playerCol, row: 0))!.update(GridStatus.p1);
        print("Adding Players 2 to the board ...");
        getGrid(GridCoordinate(col: playerCol, row: boardSize.row-1))!.update(GridStatus.p2);
    }
    
    // Get the BoardGrid with given coordinate
    fileprivate func getGrid(_ coordinate:GridCoordinate) ->  BoardGrid?{
        for child:SKNode in self.children {
            if (child.name == GameConstants.BOARDGRID_NODE_NAME) {
                let boardGrid:BoardGrid = child as! BoardGrid;
                if (boardGrid.coordinate.col == coordinate.col && boardGrid.coordinate.row == coordinate.row) {
                    return boardGrid;
                }
            }
        }
        return nil;
    }
    
    // Get all BoardGrids adjacent to the given grid
    fileprivate func getAdjacentGrids(_ sorceGrid:GridCoordinate) -> Array<BoardGrid> {
        var adjacentGrids:Array<BoardGrid> = Array<BoardGrid>();
        for child:SKNode in self.children {
            if (child.name == GameConstants.BOARDGRID_NODE_NAME) {
                let boardGrid:BoardGrid = child as! BoardGrid;
                if (BoardUtils.isAdjacent(sorceGrid, boardGrid.coordinate)) {
                    adjacentGrids.append(boardGrid);
                }
            }
        }
        return adjacentGrids;
    }
    
    // Check whether the target grid is surrounded by the removed grids
    func isSurrounded(_ sorceGrid:GridCoordinate) -> Bool {
        let adjacentGrids:Array<BoardGrid> = getAdjacentGrids(sorceGrid);
        for grid:BoardGrid in adjacentGrids {
            if (grid.gridStatus == GridStatus.idle) {
                // Still has an IDLE grid to access
                return false;
            }
        }
        // No IDLE grid accessable
        return true;
    }
    
    // For memory clean up
    func cleanUpBoard() {
        self.removeAllActions();
        self.removeAllChildren();
        print("All GameBoard Grids removed.")
    }
}
