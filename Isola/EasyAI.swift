//
//  EasyAI.swift
//  Isola
//
//  Created by Bolei Fu on 10/11/15.
//  Copyright © 2015 boleifu. All rights reserved.
//

import Foundation

class EasyAI: PlayerAI {
    
    var name = "Easy AI";
    var role:Role;
    
    var boundaryFactor:Int = 2;  // Bigger value let AI prefer staying away from the boundary
    var warningDistance:Int = 2; // Only consider the invalid block in distance
    
    init(role:Role) {
        self.role = role;
    }
    
    
    func getNextMove(_ gameBoardState:GameBoardState) -> GridCoordinate {
        return getBestMoveForPlayer(self.role, gameBoardState: gameBoardState);
    }
    
    func getNextSteal(_ gameBoardState:GameBoardState) -> GridCoordinate {
        // Block opponent's best move
        return getBestMoveForPlayer(self.role.getOpponent(), gameBoardState: gameBoardState);
    }
    
    fileprivate func getBestMoveForPlayer(_ player:Role, gameBoardState:GameBoardState) -> GridCoordinate {
        var playerGrid:GridCoordinate = GameConstants.INVALID_COORDINATE;
        var movableGrids:Array<GridCoordinate> = Array<GridCoordinate>();
        var invalidGrids:Array<GridCoordinate> = Array<GridCoordinate>();
        
        // Get player's location
        if (player == Role.p1) {
            playerGrid = gameBoardState.player1Grid;
        } else {
            playerGrid = gameBoardState.player2Grid;
        }
        
        // Analyze all grids
        let boardState = gameBoardState.boardState;
        for row:Int in 0 ..< gameBoardState.boardSize.row {
            for col:Int in 0 ..< gameBoardState.boardSize.col {
                let candidiateGridCoord:GridCoordinate = GridCoordinate(col:col, row:row);
                let candidateGridStatus:GridStatus = boardState[row][col];
                // Only check the grind in two space
                if (candidateGridStatus == GridStatus.removed
                    && BoardUtils.isInDistance(playerGrid, GridCoordinate(col: col, row: row), distance: warningDistance)) {
                        // Save Removed Grid
                        invalidGrids.append(candidiateGridCoord);
                } else {
                    if (candidateGridStatus == GridStatus.idle) {
                        if (BoardUtils.isAdjacent(playerGrid, candidiateGridCoord)) {
                            // Save adjacent movable grid
                            movableGrids.append(candidiateGridCoord);
                            //print("======Grid \(candidiateGridCoord.toString()) is Movable. Status = \(candidateGridStatus)");
                        }
                    }
                }
            }
        }
        
        // Calculate the best move
        var bestMove:GridCoordinate = GameConstants.INVALID_COORDINATE;
        var bestScore:Double = 0;
        for candidiateMove:GridCoordinate in movableGrids {
            var currentScore:Double = 0;
            // Move away from all Invalid Grids
            for invalidGrid:GridCoordinate in invalidGrids {
                let colDiff:Int = candidiateMove.col - invalidGrid.col;
                let rowDiff:Int = candidiateMove.row - invalidGrid.row;
                currentScore += sqrt(Double(colDiff * colDiff + rowDiff * rowDiff));
            }
            
            // Stay away from the boundary
            currentScore += Double(min(candidiateMove.col, gameBoardState.boardSize.col - candidiateMove.col)*boundaryFactor);
            currentScore += Double(min(candidiateMove.row, gameBoardState.boardSize.row - candidiateMove.row)*boundaryFactor);
            
            // Compare with the existing best move
            var isBetterMove:Bool = false;
            if (currentScore > bestScore) {
                isBetterMove = true;
            } else if (currentScore == bestScore) {
                // Equally good move, pick randomly
                isBetterMove = arc4random_uniform(2) == 1;
            }
            
            if (isBetterMove) {
                bestScore = currentScore;
                bestMove = candidiateMove;
            }
        }
        
        return bestMove;
    }
}
