//
//  GameUtils.swift
//  Isola
//
//  Created by Bolei Fu on 10/7/15.
//  Copyright © 2015 boleifu. All rights reserved.
//

import Foundation
import AVFoundation
import UIKit

//
// The util class contains some help functions related to the system
//
class GameUtils {
    
    static var clickSoundPlayer : AVAudioPlayer? = nil;
    
    // Get the version of the software
    static func version() -> String {
        let dictionary = Bundle.main.infoDictionary!
        let version = dictionary["CFBundleShortVersionString"] as! String
        let build = dictionary["CFBundleVersion"]as! String
        return "V \(version).\(build)"
    }
    
    static func getRandomBackgroundImage() -> UIImage? {
        let randInt:UInt32 = arc4random_uniform(4);
        if (randInt == 0) {
            return UIImage(named: "colored_castle");
        } else if (randInt == 1) {
            return UIImage(named: "colored_desert");
        } else if (randInt == 2) {
            return UIImage(named: "colored_forest");
        } else {
            return UIImage(named: "colored_talltrees");
        }
    }
    
    static func getRandomBackgroundUncoloredImage() -> UIImage? {
        let randInt:UInt32 = arc4random_uniform(8);
        if (randInt == 0) {
            return UIImage(named: "uncolored_castle");
        } else if (randInt == 1) {
            return UIImage(named: "uncolored_desert");
        } else if (randInt == 2) {
            return UIImage(named: "uncolored_forest");
        } else if (randInt == 3) {
            return UIImage(named: "uncolored_hills");
        } else if (randInt == 4) {
            return UIImage(named: "uncolored_peaks");
        } else if (randInt == 5) {
            return UIImage(named: "uncolored_piramids");
        } else if (randInt == 6) {
            return UIImage(named: "uncolored_plain");
        } else {
            return UIImage(named: "uncolored_talltrees");
        }
    }
    
    static func scaleImageToFitHeight(_ sourceImage: UIImage, targetHeight: CGFloat) -> UIImage {
        let scale:CGFloat = targetHeight/sourceImage.size.height;
        let scaledSize:CGSize = CGSize(width: sourceImage.size.width*scale, height: sourceImage.size.height*scale);
        UIGraphicsBeginImageContextWithOptions(scaledSize, false, 0);
        sourceImage.draw(in: CGRect(origin: CGPoint.zero, size: scaledSize))
        let scaledImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!;
        return scaledImage;
    }
    
    static func getRandomThemeIndex() -> UInt32 {
        return arc4random_uniform(4)
    }
    
    static func getThemeByIndex(index:  UInt32) -> Theme {
        if (index == 0) {
            return HillTheme();
        } else if (index == 1) {
            return CastleTheme();
        } else if (index == 2) {
            return ForestTheme();
        } else {
            return DesertTheme();
        }
    }
    
    static func getRandomeTheme() -> Theme {
        return getThemeByIndex(index: getRandomThemeIndex())
    }
}
