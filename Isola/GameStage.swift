//
//  GameStage.swift
//  Isola
//
//  Created by Bolei Fu on 10/7/15.
//  Copyright © 2015 boleifu. All rights reserved.
//

// This represents the stage of the in-progress game
enum GameStage{
    case not_START;
    case p1_MOVE;
    case p1_REMOVE;
    case p2_MOVE;
    case p2_REMOVE;
    case p1_WIN;
    case p2_WIN;
    
    func nextState() -> GameStage {
        switch (self) {
        case .not_START:
            return .p1_MOVE;
        case .p1_MOVE:
            return .p1_REMOVE;
        case .p1_REMOVE:
            return .p2_MOVE;
        case .p2_MOVE:
            return .p2_REMOVE;
        case .p2_REMOVE:
            return .p1_MOVE;
        default:
            return .not_START;
        }
    }
    
    func isPlayer1Stage() -> Bool {
        return self == GameStage.p1_MOVE || self == GameStage.p1_REMOVE || self == GameStage.p1_WIN;
    }
    
    func isPlayer2Stage() -> Bool {
        return self == GameStage.p2_MOVE || self == GameStage.p2_REMOVE || self == GameStage.p2_WIN;
    }

    static func getDescription(_ targetStage:GameStage) -> String {
        switch (targetStage) {
        case not_START:
            return "Touch to start the game...";
        case p1_MOVE:
            return "%@ (P1) Move";
        case p1_REMOVE:
            return "%@ (P1) Remove";
        case p2_MOVE:
            return "%@ (P2) Move";
        case p2_REMOVE:
            return "%@ (P2) Remove";
        case p1_WIN:
            return "%@ (P1) Win. Touch to start again.";
        case p2_WIN:
            return "%@ (P2) Win. Touch to start again.";
        }
    }

    static func getTurnMsg(_ targetStage:GameStage) -> String {
        switch (targetStage) {
        case not_START:
            return "Touch to start the game...";
        case p1_MOVE:
            return "%@ (P1) moved to %@";
        case p1_REMOVE:
            return "%@ (P1) removed %@";
        case p2_MOVE:
            return "%@ (P2) moved to %@";
        case p2_REMOVE:
            return "%@ (P2) removed %@";
        case p1_WIN:
            return "%@ (P1) Win";
        case p2_WIN:
            return "%@ (P2) Win";
        }
    }
}
