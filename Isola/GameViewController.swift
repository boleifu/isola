//
//  GameViewController.swift
//  Isola
//
//  Created by Bolei Fu on 10/6/15.
//  Copyright (c) 2015 boleifu. All rights reserved.
//

import UIKit
import SpriteKit
import GameKit


class GameViewController: UIViewController, GKMatchDelegate {
    enum OnlineStage {
        case CONNECTION_ESTABLISHED
        case OPPONENT_JONED
        case OPPONENT_INFO_LOADED
        case GAME_READY
        case GAME_OVER
    }
    
    @IBOutlet weak var menuButton: UIButton!
    @IBOutlet weak var menuView: UIView!
    @IBOutlet weak var resumeButton: UIButton!
    @IBOutlet weak var quitButton: UIButton!
    @IBOutlet weak var restartButton: UIButton!
    
    
    var gameView:SKView? = nil;
    var gameScene:GameScene? = nil;
    
    // Run time properties
    var boardSize:Int = 5;
    var gamePlayer1:GamePlayer? = nil
    var gamePlayer2:GamePlayer? = nil
    var gameTheme:Theme = GameUtils.getRandomeTheme();
    
    var menuHidden:Bool = true;
    
    // Online play
    var isOnlineMode = false
    var selfPlayerID = ""
    var opponentPlayerID:String = ""
    var opponentName:String = "Opponent"
    var selfInfo:PlayerInfoPacket? = nil
    var opponentInfo:PlayerInfoPacket? = nil
    
    var selfRole:Role? = nil

    var gkMatch:GKMatch? = nil
    
    var onelineGameStage:OnlineStage = .CONNECTION_ESTABLISHED
    
    @IBOutlet weak var onlineModeStatusLabel: UILabel!
    @IBOutlet weak var onlineModeDebugTextView: UITextView!

    
    override func viewDidLoad() {
        initGameView()
        initMenu()
        
        if !isOnlineMode {
            initGame()
        } else {
            initOnlineMode()
            // Online game init by the GKGame object.
        }
    }
    
    fileprivate func initMenu() {
        // Resume button
        resumeButton.titleLabel?.font = UIFont(name: "Kenvector Future", size: 15);
        resumeButton.setBackgroundImage(UIImage(named: "yellow_button04"), for: UIControlState())
        resumeButton.setBackgroundImage(UIImage(named: "yellow_button05"), for: UIControlState.highlighted)
        
        // Restart button
        restartButton.titleLabel?.font = UIFont(name: "Kenvector Future", size: 15);
        restartButton.setBackgroundImage(UIImage(named: "red_button01"), for: UIControlState())
        restartButton.setBackgroundImage(UIImage(named: "red_button02"), for: UIControlState.highlighted)
        
        // Quit button
        quitButton.titleLabel?.font = UIFont(name: "Kenvector Future", size: 15);
        quitButton.setBackgroundImage(UIImage(named: "blue_button04"), for: UIControlState())
        quitButton.setBackgroundImage(UIImage(named: "blue_button05"), for: UIControlState.highlighted)
        
        // Hide the menue view
        hideMenu();
        
        // Hide online debug text
        self.onlineModeDebugTextView.isHidden = true
    }
    
    fileprivate func initGameView() {
        // Add Game view
        gameView = self.view as? SKView;
        //gameView?.backgroundColor = GameConstants.GREEN_COLOR
        // Configure the view.
        gameView!.showsFPS = GameConfigUtils.isTestMode();
        gameView!.showsNodeCount = GameConfigUtils.isTestMode();
        gameView!.showsDrawCount = GameConfigUtils.isTestMode();
        gameView!.showsQuadCount = GameConfigUtils.isTestMode();
        gameView!.showsPhysics = GameConfigUtils.isTestMode();
        gameView!.showsFields = GameConfigUtils.isTestMode();
        gameView!.frameInterval = 5;
        
        /* Sprite Kit applies additional optimizations to improve rendering performance */
        gameView!.ignoresSiblingOrder = true;
    }
    
    fileprivate func initGame() {
        // Stop the main BGM
        AudioUtils.stopMainBgm();
        
        // Start the game BGM
        AudioUtils.playGameBgm();

        setupGameScene();
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated);
        gameScene?.removeAllChildren();
        gameScene?.removeAllActions();
        gameScene?.removeFromParent();
        gameView?.presentScene(nil);
        gameView?.removeFromSuperview();
        gameView = nil;
        gameScene = nil;
        print("GameViewController cleaned up.")
        
        // Stop the game BGM
        AudioUtils.stopGameBgm();
        
        // Restart the main BGM
        AudioUtils.playMainBgm();
    }
    
    deinit {
        print("GameViewController is deinited.");
    }

    func setupGameScene() {
        gameScene = GameScene(size: gameView!.bounds.size);
        /* Set the scale mode to scale to fit the window */
        gameScene!.scaleMode = .aspectFill;
        
        // Set game properties
        gameScene!.boardSize = boardSize;

        gameScene!.gamePlayer1 = gamePlayer1
        gameScene!.gamePlayer2 = gamePlayer2
        gameScene!.theme = gameTheme
        gameScene!.isOnlineMode = self.isOnlineMode
        gameScene!.gkMatch = self.gkMatch
        
        gameView!.presentScene(gameScene);
    }

    override var shouldAutorotate : Bool {
        return false
    }

    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    override var prefersStatusBarHidden : Bool {
        return true
    }
    
    @IBAction func clickedMenuButton(_ sender: UIButton) {
        AudioUtils.playButtonClickSound();
        if (menuHidden) {
            showMenu();
        } else {
            hideMenu();
        }
    }
    
    @IBAction func clickedResumeButton(_ sender: UIButton) {
        AudioUtils.playButtonClickSound();
        hideMenu();
        print("Resume button clicked");
    }
    
    @IBAction func clickedRestartButton(_ sender: UIButton) {
        AudioUtils.playButtonClickSound();
        gameScene?.gameCore?.resetTheGame(resetScore: true);
        hideMenu();
        print("Restart button clicked");
    }
    
    @IBAction func clickedQuitButton(_ sender: UIButton) {
        AudioUtils.playButtonClickSound();
        //self.view.window?.rootViewController?.dismissViewControllerAnimated(false, completion: nil);
        self.dismiss(animated: true, completion: nil);
        
        if isOnlineMode {
            // Lose the game if it is not over yet.
            quitOnlineMatch(winGame: false)
        }
    }
    
    fileprivate func hideMenu() {
        // Disable menu view
        menuView.isUserInteractionEnabled = false;
        menuView.isHidden = true;
        gameView?.isPaused = false;
        if (gameScene != nil) {
            gameScene!.touchDisabled = false;   
        }
        menuHidden = true;
        
    }
    
    fileprivate func showMenu() {
        // Disable menu view
        menuView.isUserInteractionEnabled = true;
        menuView.isHidden = false;
        if !isOnlineMode {
            // Online game cannot be paused.
            gameView?.isPaused = true;
        }
        gameScene?.touchDisabled = true;
        menuHidden = false;
    }
    
    fileprivate func updateonlineModeStatusText(_ text:String) {
        DispatchQueue.main.async {
            self.onlineModeStatusLabel.text = text;
        };
    }
    
    // Online game
    
    fileprivate func initOnlineMode() {
        resumeButton.isEnabled = false
        restartButton.isEnabled = false
        
        onlineModeStatusLabel.font = UIFont(name: "Kenvector Future Thin", size: 20);
        onlineModeStatusLabel.textColor = GameConstants.RED_COLOR
        if onelineGameStage == .CONNECTION_ESTABLISHED {
            onlineModeStatusLabel.text = "Wait for opponent to join..."
        }
    }
    
    fileprivate func changeOnlineGameStage(_ currStage: OnlineStage) {
        onelineGameStage = currStage
        DispatchQueue.main.async {
            if self.onelineGameStage == .GAME_READY {
                self.onlineModeStatusLabel.isHidden = true
            } else {
                self.onlineModeStatusLabel.isHidden = false
            }
        }
    }
    
    fileprivate func quitOnlineMatch(winGame:Bool?) {
        if gkMatch != nil {
            print("Quiting online match.")
            gkMatch!.disconnect()
            // Win the game if started.
            gameScene?.onelineGameOver(winGame);
            //AudioUtils.playGameOverSound()
            //gameScene?.removeAllChildren();
            //gameScene?.removeAllActions();
            //gameScene?.removeFromParent();
        }
    }
    
    fileprivate func sendPlayerInfoMsg() {
        if selfInfo == nil {
            print("No self info packet found, unable to send player info message.")
            showNetworkError()
            return
        }

        let data = PacketHelper.encodePlayerInfoPacket(packet: selfInfo!)
        do {
            print("Sending player info message.")
            try gkMatch?.sendData(toAllPlayers: data, with: GKMatchSendDataMode.reliable)
        } catch {
            showNetworkError()
            print("Failed to send player info message: \(error)")
        }
    }
    
    fileprivate func prepareOnlineGame() {
        // Figure out use whose proposal
        if winRoll() {
            // Win the roll, use my proposal
            print("Win the roll.")
            selfRole = Role.p1
            boardSize = Int(selfInfo!.boardWidth)
            gameTheme = GameUtils.getThemeByIndex(index: selfInfo!.gameThemeIndex)
            gamePlayer1 = PlayerFactory.getOnlinePlayer(iconIndex: selfInfo!.playrIconIndex, role: Role.p1, playerName: "ME", playerScore: selfInfo!.playerScore, playerWinNum: selfInfo!.playerWinNum, playerLostNum: selfInfo!.playerLostNum, isOpponent: false)
            gamePlayer2 = PlayerFactory.getOnlinePlayer(iconIndex: opponentInfo!.playrIconIndex, role: Role.p2, playerName: opponentName, playerScore: opponentInfo!.playerScore, playerWinNum: opponentInfo!.playerWinNum, playerLostNum: opponentInfo!.playerLostNum, isOpponent: true)
        } else {
            print("Lost the roll.")
            selfRole = Role.p2
            boardSize = Int(opponentInfo!.boardWidth)
            gameTheme = GameUtils.getThemeByIndex(index: opponentInfo!.gameThemeIndex)
            gamePlayer1 = PlayerFactory.getOnlinePlayer(iconIndex: opponentInfo!.playrIconIndex, role: Role.p1, playerName: opponentName, playerScore: opponentInfo!.playerScore, playerWinNum: opponentInfo!.playerWinNum, playerLostNum: opponentInfo!.playerLostNum, isOpponent: true)
            gamePlayer2 = PlayerFactory.getOnlinePlayer(iconIndex: selfInfo!.playrIconIndex, role: Role.p2, playerName: "ME", playerScore: selfInfo!.playerScore, playerWinNum: selfInfo!.playerWinNum, playerLostNum: selfInfo!.playerLostNum, isOpponent: false)
        }
        
        // Debug
        /*
        self.onlineModeDebugTextView.isHidden = false
        DispatchQueue.main.async {
            self.onlineModeDebugTextView.text = "----\n" +
            "Self: \(self.selfPlayerID)\n" +
            "Score: \(self.selfInfo!.playerScore) W \(self.selfInfo!.playerWinNum) L \(self.selfInfo!.playerLostNum)\n" +
            "Roll: \(self.selfInfo!.rollNum)\n" +
            "----\n" +
            "Opponent: \(self.opponentName)[\(self.opponentPlayerID)]" +
            "Score: \(self.opponentInfo!.playerScore) W \(self.opponentInfo!.playerWinNum) L \(self.opponentInfo!.playerLostNum)\n" +
            "Roll: \(self.opponentInfo!.rollNum)\n" +
            "========\n" +
            "Game Info:\n" +
            "Self Role: \(self.selfRole!)\n" +
            "Board Size: \(self.boardSize), Player1IconIndex: \(self.gamePlayer1!.playerIconIndex), Player2IconIndex: \(self.gamePlayer2!.playerIconIndex)\n" +
            "gameTheme: \(self.gameTheme)"
            
        };
        */
    }

    fileprivate func winRoll() -> Bool {
        if (selfInfo!.rollNum == opponentInfo!.rollNum) {
            return selfPlayerID > opponentPlayerID
        }
        
        return selfInfo!.rollNum > opponentInfo!.rollNum
        
    }
    
    fileprivate func showNetworkError() {
        updateonlineModeStatusText("Network error")
    }

    // GKMatchDelegate
    
    func match(_ match: GKMatch, player: GKPlayer, didChange state: GKPlayerConnectionState) {
        switch (state)
        {
        case GKPlayerConnectionState.stateConnected:
            // Handle a new player connection.
            print("Player \(String(describing: player.playerID)) \(opponentName) connected.")
            
            if player.playerID == nil {
                print("Invalid player.")
                updateonlineModeStatusText("Invalid Player")
                break;
            }
            
            updateonlineModeStatusText("Opponent joined")
            opponentPlayerID = player.playerID!
            if player.displayName != nil {
                opponentName = player.displayName!
            }
            changeOnlineGameStage(OnlineStage.OPPONENT_JONED)
            gkMatch = match
            sendPlayerInfoMsg()
            //initGame()
            break;
        case GKPlayerConnectionState.stateDisconnected:
            // A player just disconnected.
            print("Player \(String(describing: player.playerID)) \(opponentName) disconnected.")
            updateonlineModeStatusText("Opponent disconnected")
            quitOnlineMatch(winGame: true)
            changeOnlineGameStage(OnlineStage.GAME_OVER)
            break;
        case GKPlayerConnectionState.stateUnknown:
            // A player is in unknown state.
            print("Player \(String(describing: player.playerID)) \(opponentName) state unknown.")
            changeOnlineGameStage(OnlineStage.GAME_OVER)
            showNetworkError()
            break;
            
        }
    }
    
    func match(_ match: GKMatch, didReceive data: Data, fromRemotePlayer player: GKPlayer){
        print("Data received from \(String(describing: player.displayName))")
        
        if onelineGameStage == .OPPONENT_JONED {
            opponentInfo = PacketHelper.decodePlayerInfoPacket(data: data)
            print("\(opponentName) \(String(describing: opponentInfo))")
            prepareOnlineGame()
            updateonlineModeStatusText("Loaded opponent profile")
            
            changeOnlineGameStage(OnlineStage.GAME_READY)
            // Start the game
            initGame()
        } else if (onelineGameStage == .GAME_READY) {
            let opponentPlay:PlayerTurnPacket = PacketHelper.decodePlayerTurnPacket(data: data)!
            
            // Check whether game is in sync.
            // TODO: Handle unsync case. Maybe full sync.
            if gameScene?.currentTurnNum != opponentPlay.turnNum {
                print("Game out-of-sync. Expected: \(String(describing: gameScene?.currentTurnNum)), Actual: \(opponentPlay.turnNum)")
                match.disconnect()
            }
            
            let opponentPlayCoord = GridCoordinate(col: opponentPlay.col, row: opponentPlay.row)
            print("\(opponentName)'s play: \(opponentPlayCoord)")
            self.gameScene?.opponentPlay = opponentPlayCoord
        } else {
            print("Invalid stage: \(onelineGameStage)")
        }
    }
    
    func match(_ match: GKMatch, didFailWithError error: Error?) {
        print("Game connectiong error. \(String(describing: error?.localizedDescription))")
        changeOnlineGameStage(OnlineStage.GAME_OVER)
        showNetworkError()
        quitOnlineMatch(winGame: nil)
    }
}
