//
//  OnlinePlayPrepViewController.swift
//  Zoo Battle
//
//  Created by Bolei Fu on 8/24/17.
//  Copyright © 2017 boleifu. All rights reserved.
//

import UIKit
import GameKit

class OnlinePlayPrepViewController: UIViewController, GKGameCenterControllerDelegate {
    
    enum GameMakerState {
        case NOT_START
        case SEARCHING
        case IN_GAME
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var playerIconImage: UIImageView!
    @IBOutlet weak var iconNameLabel: UILabel!

    @IBOutlet weak var iconLeftButton: UIButton!
    @IBOutlet weak var iconRightButton: UIButton!
    
    @IBOutlet weak var battleScoreLabel: UILabel!
    @IBOutlet weak var winNumberLabel: UILabel!
    @IBOutlet weak var lostNumberLabel: UILabel!
    
    @IBOutlet weak var battleScoreValueLabel: UILabel!
    @IBOutlet weak var winNumberValueLabel: UILabel!
    @IBOutlet weak var lostNumberValueLabel: UILabel!
    
    @IBOutlet weak var currentStatusLabel: UILabel!

    @IBOutlet weak var startGameButton: UIButton!
    @IBOutlet weak var backButton: UIButton!

    var backgroundImageView:UIImageView = UIImageView();
    var moveXOffset:CGFloat = 0;
    
    // Game stats
    var gcReady = false
    var playerIconIndex:Int = Int(PlayerIconUtils.getRandomIconIndex());
    var matchMakerState = GameMakerState.NOT_START
    
    // Player stats
    var playerID:String = ""
    var playerScore:Int = 0
    var playerWinNum:Int = 0
    var playerLostNum:Int = 0
    
    var scoreLoaded = false
    var winNumLoaded = false
    var lostNumLoaded = false
    
    var gameReady = false

    deinit {
        NotificationCenter.default.removeObserver(self);
        print("OnlinePlayPrepViewController is deinited.");
    }
    
    override func viewDidLoad() {
        super.viewDidLoad();

        // Start animation when comes to the forground
        NotificationCenter.default.addObserver(self, selector: #selector(OnlinePlayPrepViewController.enterForeground(_:)), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil);
        
        // Background image view
        backgroundImageView = UIImageView();
        backgroundImageView.contentMode = UIViewContentMode.scaleAspectFill;
        backgroundImageView.layer.zPosition = -1;
        self.view.addSubview(backgroundImageView);
        
        // View title label
        titleLabel.font = UIFont(name: "Kenvector Future", size: 20);
        
        // Player icon
        iconNameLabel.font = UIFont(name: "Kenvector Future Thin", size: 10);
        iconNameLabel.textColor = UIColor.gray;
        setPlayerIconImage(playerIconIndex)
        
        // Player stats
        battleScoreLabel.font = UIFont(name: "Kenvector Future Thin", size: 15);
        winNumberLabel.font = UIFont(name: "Kenvector Future Thin", size: 15);
        lostNumberLabel.font = UIFont(name: "Kenvector Future Thin", size: 15);
        
        battleScoreValueLabel.font = UIFont(name: "Kenvector Future Thin", size: 15);
        winNumberValueLabel.font = UIFont(name: "Kenvector Future Thin", size: 15);
        lostNumberValueLabel.font = UIFont(name: "Kenvector Future Thin", size: 15);

        // Start game button
        startGameButton.titleLabel?.font = UIFont(name: "Kenvector Future", size: 20);
        startGameButton.setBackgroundImage(UIImage(named: "green_button00"), for: UIControlState())
        startGameButton.setBackgroundImage(UIImage(named: "green_button01"), for: UIControlState.highlighted)
        
        // Back button
        backButton.titleLabel?.font = UIFont(name: "Kenvector Future", size: 20);
        backButton.setBackgroundImage(UIImage(named: "red_button01"), for: UIControlState())
        backButton.setBackgroundImage(UIImage(named: "red_button02"), for: UIControlState.highlighted)
        
        // Status label
        currentStatusLabel.font = UIFont(name: "Kenvector Future Thin", size: 15);
        currentStatusLabel.text = ""
    }

    override func viewWillAppear(_ animated:Bool) {
        super.viewWillAppear(animated);
        
        startAnimation();
        loadPlayerInfo();
    }

    @objc fileprivate func enterForeground(_ notification: Notification) {
        startAnimation();
    }
    
    fileprivate func setPlayerIconImage(_ iconIndex:Int) {
        playerIconImage.image = PlayerIconUtils.getImageOfIcon(iconIndex);
        iconNameLabel.text = PlayerIconUtils.getIconName(iconIndex);
    }
    
    // Button actions
    @IBAction func clickedIconLeftButton(_ sender: UIButton) {
        AudioUtils.playButtonClickSound();
        playerIconIndex = PlayerIconUtils.getPrevIconIndex(playerIconIndex);
        setPlayerIconImage(playerIconIndex);
    }
    
    @IBAction func clickedIconRightButton(_ sender: UIButton) {
        AudioUtils.playButtonClickSound();
        playerIconIndex = PlayerIconUtils.getNextIconIndex(playerIconIndex);
        setPlayerIconImage(playerIconIndex);
    }

    @IBAction func clickedButton(_ sender: UIButton) {
        AudioUtils.playButtonClickSound();
        // Back button clicked
        if (sender.restorationIdentifier == "BackButton") {
            self.dismiss(animated: true, completion: nil);
            
            // Cancel match search
            if (self.matchMakerState == GameMakerState.SEARCHING) {
                GKMatchmaker.shared().cancel()
                print("Canceled game searching.")
            }
        }
    }
    
    fileprivate func startAnimation() {
        // Init animated object satus
        initBackgroundImage();
        
        // Move the bg image horizontally
        UIView.animate(withDuration: 10, delay: 0, options: [UIViewAnimationOptions.repeat, UIViewAnimationOptions.autoreverse] , animations: {
            self.backgroundImageView.frame = CGRect(
                origin: CGPoint(x: -self.moveXOffset, y: 0),
                size: self.backgroundImageView.frame.size);
        },
                       completion: nil);
    }
    
    fileprivate func initBackgroundImage() {
        // Background image
        let backgroundImage:UIImage = GameUtils.getRandomBackgroundUncoloredImage()!;
        
        // Scale image
        let scaledImage:UIImage = GameUtils.scaleImageToFitHeight(backgroundImage, targetHeight: self.view.bounds.size.height);
        backgroundImageView.image = scaledImage;
        moveXOffset = abs(scaledImage.size.width-self.view.frame.size.width)/2;
        backgroundImageView.frame = CGRect(origin: CGPoint(x: moveXOffset, y: 0), size: self.view.frame.size);
    }
    
    // Game Center
    
    // Clear locally saved playerInfo
    fileprivate func clearPlayerInfo() {
        startGameButton.isEnabled = false
        battleScoreValueLabel.text = "--"
        winNumberValueLabel.text = "--"
        lostNumberValueLabel.text = "--"
        
        playerScore = 0
        playerWinNum = 0
        playerLostNum = 0
        
        scoreLoaded = false
        winNumLoaded = false
        lostNumLoaded = false
        gameReady = false
    }
    
    fileprivate func loadPlayerInfo() {
        clearPlayerInfo()
        
        let localPlayer = GKLocalPlayer.localPlayer()
        print("Checking player login status: \(localPlayer.isAuthenticated)")
        gcReady = localPlayer.isAuthenticated && localPlayer.playerID != nil
        
        if (gcReady) {
            currentStatusLabel.text = "Loading..."
            playerID = localPlayer.playerID!
            loadPlayerScoreFromLB(player: localPlayer, leaderBoardID: GameConstants.SCORE_LEADER_BOARD_ID)
            loadPlayerScoreFromLB(player: localPlayer, leaderBoardID: GameConstants.WIN_LEADER_BOARD_ID)
            loadPlayerScoreFromLB(player: localPlayer, leaderBoardID: GameConstants.LOSE_LEADER_BOARD_ID)
        } else {
            currentStatusLabel.text = "Game Center not ready..."
            print("Game Center not ready, unable to load the player.")
        }
    }
    
    fileprivate func loadPlayerScoreFromLB(player: GKPlayer, leaderBoardID: String) {
        let lb = GKLeaderboard(players: [player])
        lb.identifier = leaderBoardID
        lb.timeScope = GKLeaderboardTimeScope.allTime
        
        // Load score
        lb.loadScores(completionHandler: { (gkScores, error) in
            if error != nil {
                print("Failed to load the score in leaderboard \(leaderBoardID). \(error!.localizedDescription)")
                return
            }
            
            if gkScores == nil || gkScores!.count <= 0 {
                print("No score for player \(String(describing: player.displayName)) in leaderboard \(leaderBoardID)")
                self.loadedScore(score: 0, leaderBoardID: leaderBoardID)
                return
            }
            
            print("Loaded the leaderboard \(leaderBoardID), scores num \(gkScores!.count)")
            self.loadedScore(score: Int(gkScores![0].value), leaderBoardID: leaderBoardID)
        })
    }
    
    fileprivate func loadedScore(score:Int, leaderBoardID: String) {
        if leaderBoardID == GameConstants.SCORE_LEADER_BOARD_ID {
            print("playerScore = \(score)")
            playerScore = score
            battleScoreValueLabel.text = String(playerScore)
            scoreLoaded = true
        }
        
        if leaderBoardID == GameConstants.WIN_LEADER_BOARD_ID {
            print("playerWinNum = \(score)")
            playerWinNum = score
            winNumberValueLabel.text = String(playerWinNum)
            winNumLoaded = true
        }
        
        if leaderBoardID == GameConstants.LOSE_LEADER_BOARD_ID {
            print("playerLostNum = \(score)")
            playerLostNum = score
            lostNumberValueLabel.text = String(playerLostNum)
            lostNumLoaded = true
        }
        
        gameReady = scoreLoaded && winNumLoaded && lostNumLoaded
        if gameReady {
            currentStatusLabel.text = ""
            startGameButton.isEnabled = true
        }
    }
    
    func gameCenterViewControllerDidFinish(_ gameCenterViewController: GKGameCenterViewController)
    {
        gameCenterViewController.dismiss(animated: true, completion: nil)
    }

    @IBAction func checkGCLeaderboard(_ sender: UIButton) {
        let gcVC = GKGameCenterViewController()
        gcVC.gameCenterDelegate = self
        gcVC.viewState = .leaderboards
        
        if sender.restorationIdentifier == "score_leader_board" {
            gcVC.leaderboardIdentifier = GameConstants.SCORE_LEADER_BOARD_ID
        } else if sender.restorationIdentifier == "win_leader_board" {
            gcVC.leaderboardIdentifier = GameConstants.WIN_LEADER_BOARD_ID
        }
        
        present(gcVC, animated: true, completion: nil)
    }
    
    @IBAction func requrestInstantMatch(_ sender: UIButton) {
        let r = GKMatchRequest()
        r.minPlayers = 2
        r.maxPlayers = 2
        
        print("Searching an instant game...")
        currentStatusLabel.text = "Searching for a game..."
        startGameButton.isEnabled = false
        self.matchMakerState = GameMakerState.SEARCHING
        
        // Search for a match
        GKMatchmaker.shared().findMatch(
            for: r,
            withCompletionHandler: {(match : GKMatch?, error: Error?) -> Void in
                if (error != nil) {
                    print("Failed to find an instant match. Error: \(String(describing: error))")
                    self.currentStatusLabel.text = "Unable to find a game."
                    return
                }
                
                if (match == nil) {
                    print("Unable to find an instant match.")
                    self.currentStatusLabel.text = "Unable to find a game."
                    return
                }
                
                print("Found match.")
                if (self.matchMakerState != GameMakerState.IN_GAME) {
                    self.matchMakerState = GameMakerState.IN_GAME
                    print("Game is going to start...")
                    self.currentStatusLabel.text = "Game is going to start..."
                    self.startOnlineGame(match: match!)
                }
                
        })
    }
    
    fileprivate func startOnlineGame(match : GKMatch) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let gameVC = storyboard.instantiateViewController(withIdentifier: "GameViewController") as! GameViewController
        let rollNum = arc4random_uniform(100)
        let boardWidth = 5 + arc4random_uniform(4)
        gameVC.isOnlineMode = true;
        gameVC.selfPlayerID = playerID
        gameVC.selfInfo = PlayerInfoPacket(playrIconIndex: playerIconIndex, playerScore:playerScore, playerWinNum:playerWinNum, playerLostNum: playerLostNum, rollNum:rollNum, boardWidth: boardWidth, gameThemeIndex:GameUtils.getRandomThemeIndex())
        
        print("Starting GameViewController")
        self.present(gameVC, animated: true, completion: nil)
        startGameButton.isEnabled = true
        self.matchMakerState = GameMakerState.NOT_START
        self.currentStatusLabel.text = ""
        match.delegate = gameVC
    }
}
