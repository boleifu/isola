//
//  BottomHudBoard.swift
//  Zoo Battle
//
//  Created by Bolei Fu on 8/29/17.
//  Copyright © 2017 boleifu. All rights reserved.
//

import SpriteKit

//
// The HUD node that shows player status
//
class BottomHudBoard : SKSpriteNode {
    unowned let player1:GamePlayer
    unowned let player2:GamePlayer
    
    // HUD contents
    fileprivate var player1Icon:SKSpriteNode? = nil;
    fileprivate var player2Icon:SKSpriteNode? = nil;
    
    fileprivate var gameTitleLabel:SKLabelNode? = nil;
    fileprivate var gameScoreLabel:SKLabelNode? = nil;

    fileprivate var player1NameLabel:SKLabelNode? = nil;
    fileprivate var player2NameLabel:SKLabelNode? = nil;

    fileprivate var gameStageLabel:SKLabelNode? = nil;
    
    // Online Mode
    let isOnlineMode:Bool
    
    fileprivate var timeCountDownLabel:SKLabelNode? = nil;
    fileprivate var player1InfoLabel:SKLabelNode? = nil;
    fileprivate var player2InfoLabel:SKLabelNode? = nil;

    // Constructor
    init(size:CGSize, player1:GamePlayer, player2:GamePlayer, onlineMode:Bool) {
        self.player1 = player1
        self.player2 = player2
        self.isOnlineMode = onlineMode
        super.init(texture: nil, color: SKColor.white, size: size);
        
        // Set up the game board
        self.name = GameConstants.BOTTOM_HUDBOARD_NODE_NAME;
        self.texture?.filteringMode = SKTextureFilteringMode.nearest;
        self.color = SKColor.clear;
        
        
        setupHudBoard();
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        self.removeAllActions();
        self.removeAllChildren();
        print("BottomHudBoard is deinited.")
    }

    // Set up the HudBoard
    fileprivate func setupHudBoard() {
        // Setup Player Icon label
        player1Icon = SKSpriteNode(imageNamed: PlayerIconUtils.getIconName(player1.playerIconIndex))
        player1Icon!.anchorPoint = CGPoint(x: 0, y: 1)
        player1Icon!.position = CGPoint(x: -self.size.width/9*4, y: -self.size.height/9)
        player1Icon!.size = CGSize(width: self.size.height/4, height: self.size.height/4)
        player1Icon!.zPosition = CGFloat(LayerDepth.hudboard.rawValue);
        self.addChild(player1Icon!)
        
        player2Icon = SKSpriteNode(imageNamed: PlayerIconUtils.getIconName(player2.playerIconIndex))
        player2Icon!.anchorPoint = CGPoint(x: 1, y: 1)
        player2Icon!.position = CGPoint(x: self.size.width/9*4, y: -self.size.height/9)
        player2Icon!.size = CGSize(width: self.size.height/4, height: self.size.height/4)
        player2Icon!.zPosition = CGFloat(LayerDepth.hudboard.rawValue);
        self.addChild(player2Icon!)
        
        let player1IconName:String = PlayerIconUtils.getIconName(player1.playerIconIndex).capitalized;
        let player2IconName:String = PlayerIconUtils.getIconName(player2.playerIconIndex).capitalized;
        
        // Setup Game title
        gameTitleLabel = SKLabelNode(fontNamed: "KenVector Future Regular");
        gameTitleLabel!.text = "\(player1IconName) VS \(player2IconName)"
        gameTitleLabel!.fontSize = 15;
        gameTitleLabel!.fontColor = SKColor.black;
        gameTitleLabel!.position = CGPoint(x: 0, y: -self.size.height/9-player1Icon!.size.height/2);
        gameTitleLabel!.zPosition = CGFloat(LayerDepth.hudboard.rawValue);
        gameTitleLabel!.horizontalAlignmentMode = .center
        gameTitleLabel!.verticalAlignmentMode = .center
        self.addChild(gameTitleLabel!)
        
        // Setup Player name labels
        player1NameLabel = SKLabelNode(fontNamed: "KenVector Future Regular");
        player1NameLabel!.text = player1.playerName
        player1NameLabel!.fontSize = 15;
        player1NameLabel!.fontColor = GameConstants.RED_COLOR;
        player1NameLabel!.position = CGPoint(x: -self.size.width/9*4, y: -self.size.height/2);
        player1NameLabel!.zPosition = CGFloat(LayerDepth.hudboard.rawValue);
        player1NameLabel!.horizontalAlignmentMode = .left
        player1NameLabel!.verticalAlignmentMode = .top
        self.addChild(player1NameLabel!)
        
        player2NameLabel = SKLabelNode(fontNamed: "KenVector Future Regular");
        player2NameLabel!.text = player2.playerName
        player2NameLabel!.fontSize = 15;
        player2NameLabel!.fontColor = GameConstants.BLUE_COLOR;
        player2NameLabel!.position = CGPoint(x: self.size.width/9*4, y: -self.size.height/2);
        player2NameLabel!.zPosition = CGFloat(LayerDepth.hudboard.rawValue);
        player2NameLabel!.horizontalAlignmentMode = .right
        player2NameLabel!.verticalAlignmentMode = .top
        self.addChild(player2NameLabel!)
        
        // Setup game stage label
        gameStageLabel = SKLabelNode(text: "Game Stage");
        gameStageLabel!.fontName = "ArialRoundedMTBold";
        gameStageLabel!.fontSize = 15;
        gameStageLabel!.fontColor = GameConstants.BLACK_COLOR;
        gameStageLabel!.position = CGPoint(x: 0, y: -self.size.height*7/8);
        gameStageLabel!.zPosition = CGFloat(LayerDepth.hudboard.rawValue);
        self.addChild(gameStageLabel!);

        if !isOnlineMode {
            // Setup Game Score
            gameScoreLabel = SKLabelNode(fontNamed: "KenVector Future Regular");
            gameScoreLabel!.text = "0 : 0"
            gameScoreLabel!.fontSize = 20;
            gameScoreLabel!.fontColor = GameConstants.BLACK_COLOR;
            gameScoreLabel!.position = CGPoint(x: 0, y: -self.size.height/2);
            gameScoreLabel!.zPosition = CGFloat(LayerDepth.hudboard.rawValue);
            gameScoreLabel!.horizontalAlignmentMode = .center
            gameScoreLabel!.verticalAlignmentMode = .center
            self.addChild(gameScoreLabel!)
        } else {
            // Time count down
            timeCountDownLabel = SKLabelNode(fontNamed: "KenVector Future Regular");
            timeCountDownLabel!.text = String(GameConstants.GAME_TURN_TIME)
            timeCountDownLabel!.fontSize = 25;
            timeCountDownLabel!.fontColor = GameConstants.BLACK_COLOR;
            timeCountDownLabel!.position = CGPoint(x: 0, y: -self.size.height/2);
            timeCountDownLabel!.zPosition = CGFloat(LayerDepth.hudboard.rawValue);
            timeCountDownLabel!.horizontalAlignmentMode = .center
            timeCountDownLabel!.verticalAlignmentMode = .center
            self.addChild(timeCountDownLabel!)
            
            // Player1 Info
            player1InfoLabel = SKLabelNode(fontNamed: "KenVector Future Thin");
            player1InfoLabel!.text = "Score:\(player1.playerScore) W:\(player1.playerWinNum) L:\(player1.playerLostNum)"
            player1InfoLabel!.fontSize = 10;
            player1InfoLabel!.fontColor = GameConstants.RED_COLOR;
            player1InfoLabel!.position = CGPoint(x: -self.size.width/9*4, y: -self.size.height/5*3);
            player1InfoLabel!.zPosition = CGFloat(LayerDepth.hudboard.rawValue);
            player1InfoLabel!.horizontalAlignmentMode = .left
            player1InfoLabel!.verticalAlignmentMode = .top
            self.addChild(player1InfoLabel!)
            
            // Player2 Info
            player2InfoLabel = SKLabelNode(fontNamed: "KenVector Future Thin");
            player2InfoLabel!.text = "Score:\(player2.playerScore) W:\(player2.playerWinNum) L:\(player2.playerLostNum)"
            player2InfoLabel!.fontSize = 10;
            player2InfoLabel!.fontColor = GameConstants.BLUE_COLOR;
            player2InfoLabel!.position = CGPoint(x: self.size.width/9*4, y: -self.size.height/5*3);
            player2InfoLabel!.zPosition = CGFloat(LayerDepth.hudboard.rawValue);
            player2InfoLabel!.horizontalAlignmentMode = .right
            player2InfoLabel!.verticalAlignmentMode = .top
            self.addChild(player2InfoLabel!)
        }
        
        // No player plays turn right now.
        setActivePlayer(role: nil)
    }
    
    // Set player which is currently playing the turn.
    func setActivePlayer(role:Role?) {
        player1Icon?.alpha = 0.5
        player2Icon?.alpha = 0.5
        player1Icon?.xScale = 1
        player1Icon?.yScale = 1
        player2Icon?.xScale = 1
        player2Icon?.yScale = 1
        
        if (role == nil) {
            return
        }
        
        if (role == Role.p1) {
            player1Icon?.alpha = 1
            player1Icon?.xScale = 1.5
            player1Icon?.yScale = 1.5
        } else {
            player2Icon?.alpha = 1
            player2Icon?.xScale = 1.5
            player2Icon?.yScale = 1.5
        }
    }
    
    // Update the score board.
    func refreshScoreBoard(p1Score:Int, p2Score:Int) {
        gameScoreLabel!.text = "\(p1Score) : \(p2Score)"
    }
    
    // Update the current game stage message
    func updateGameStage(_ newStageMsg:String) {
        gameStageLabel!.text = newStageMsg;
    }
    
    func updateCountDown(_ timeRemains: TimeInterval) {
        let countDownSec =  Int(max(timeRemains, 0))
        if countDownSec <= 5 {
            timeCountDownLabel?.fontColor = GameConstants.RED_COLOR
            timeCountDownLabel?.fontSize = 35
        } else {
            timeCountDownLabel?.fontColor = GameConstants.BLACK_COLOR
            timeCountDownLabel?.fontSize = 25
        }
        timeCountDownLabel?.text = "\(countDownSec)"
    }
}
