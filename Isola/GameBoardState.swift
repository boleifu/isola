//
//  GameBoardState.swift
//  Isola
//
//  Created by Bolei Fu on 10/10/15.
//  Copyright © 2015 boleifu. All rights reserved.
//

import Foundation

class GameBoardState {
    let player1Grid : GridCoordinate;
    let player2Grid: GridCoordinate;
    let boardSize:BoardSize;
    let boardState: Array<Array<GridStatus>>; // boardState[row][col]
    
    init (player1Grid: GridCoordinate, player2Grid: GridCoordinate, boardSize:BoardSize, boardState:Array<Array<GridStatus>>) {
        self.player1Grid = player1Grid;
        self.player2Grid = player2Grid;
        self.boardSize = boardSize;
        self.boardState = boardState;
    }
    
}
