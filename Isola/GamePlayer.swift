//
//  GamePlayer.swift
//  Zoo Battle
//
//  Created by Bolei Fu on 8/29/17.
//  Copyright © 2017 boleifu. All rights reserved.
//

import Foundation

class GamePlayer {
    let playerIconIndex:Int
    let playerName:String
    let playerAI:PlayerAI?
    let role:Role

    // Online player only
    let isOnline:Bool
    let playerScore:Int
    let playerWinNum:Int
    let playerLostNum:Int
    
    init(playerIconIndex:Int, playerName:String, playerAI:PlayerAI?, role:Role,
         isOnline:Bool, playerScore:Int, playerWinNum:Int, playerLostNum:Int) {
        self.playerIconIndex = playerIconIndex
        self.playerName = playerName
        self.playerAI = playerAI
        self.role = role
        self.isOnline = isOnline
        self.playerScore = playerScore
        self.playerWinNum = playerWinNum
        self.playerLostNum = playerLostNum
    }
}

class PlayerFactory {
    static func getEasyAIPlayer(iconIndex:Int, role:Role) -> GamePlayer {
        let playerAI = PlayerAIEnum.findPlayerAI(GameConstants.EASY_AI).getPlayerAI(role)
        return GamePlayer(playerIconIndex: iconIndex, playerName: "Easy AI", playerAI: playerAI, role: role, isOnline:false, playerScore: 0, playerWinNum: 0, playerLostNum: 0);
    }
    
    static func getHardAIPlayer(iconIndex:Int, role:Role) -> GamePlayer {
        let playerAI = PlayerAIEnum.findPlayerAI(GameConstants.HARD_AI).getPlayerAI(role)
        return GamePlayer(playerIconIndex: iconIndex, playerName: "Hard AI", playerAI: playerAI, role: role, isOnline:false, playerScore: 0, playerWinNum: 0, playerLostNum: 0);
    }
    
    static func getOfflineHumanPlayer(iconIndex:Int, role:Role) -> GamePlayer {
        return GamePlayer(playerIconIndex: iconIndex, playerName: "Player", playerAI: nil, role: role, isOnline:false, playerScore: 0, playerWinNum: 0, playerLostNum: 0);
    }
    
    static func getOfflinePlayer(iconIndex:Int, role:Role, playerType:String) -> GamePlayer {
        if (playerType == GameConstants.EASY_AI) {
            return PlayerFactory.getEasyAIPlayer(iconIndex:iconIndex, role:role)
        } else if (playerType == GameConstants.HARD_AI) {
            return PlayerFactory.getHardAIPlayer(iconIndex:iconIndex, role:role)
        } else {
            return PlayerFactory.getOfflineHumanPlayer(iconIndex:iconIndex, role:role)
        }
    }
    
    static func getOnlinePlayer(iconIndex:Int, role:Role, playerName:String, playerScore:Int, playerWinNum:Int, playerLostNum:Int, isOpponent:Bool) -> GamePlayer {
        return GamePlayer(playerIconIndex: iconIndex, playerName: playerName, playerAI: nil, role: role, isOnline:isOpponent, playerScore: playerScore, playerWinNum: playerWinNum, playerLostNum: playerLostNum)
    }
    
}
