//
//  GamePrepViewController.swift
//  Isola
//
//  Created by Bolei Fu on 10/13/15.
//  Copyright © 2015 boleifu. All rights reserved.
//

import UIKit

class GamePrepViewController: UIViewController{
    
    let playerAvailable = [GameConstants.HUMANPLAYER, GameConstants.EASY_AI, GameConstants.HARD_AI];
    
    @IBOutlet weak var viewTitleLabel: UILabel!
    @IBOutlet weak var player1TitleLabel: UILabel!
    @IBOutlet weak var player2TitleLabel: UILabel!
    @IBOutlet weak var vsLabel: UILabel!
    @IBOutlet weak var IconSelectionNoteLabel: UILabel!
    
    @IBOutlet weak var player1IconNameLabel: UILabel!
    @IBOutlet weak var player2IconNameLabel: UILabel!
    
    @IBOutlet weak var player1IconButton: UIButton!
    @IBOutlet weak var player2IconButton: UIButton!
    
    
    @IBOutlet weak var player1Label: UILabel!
    @IBOutlet weak var player1Selector: UISegmentedControl!
    
    
    @IBOutlet weak var player2Label: UILabel!
    @IBOutlet weak var player2Selector: UISegmentedControl!
    
    @IBOutlet weak var boardSizeTitleLabel: UILabel!
    @IBOutlet weak var boardSizeLabel: UILabel!
    @IBOutlet weak var gameBoardSlider: UISlider!
    
    @IBOutlet weak var startGameButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    
    var backgroundImageView:UIImageView = UIImageView();
    var moveXOffset:CGFloat = 0;
    
    var player1IconIndex:Int = Int(PlayerIconUtils.getRandomIconIndex());
    var player2IconIndex:Int = Int(PlayerIconUtils.getRandomIconIndex());
    
    deinit {
        NotificationCenter.default.removeObserver(self);
        print("GamePrepViewController is deinited.");
    }
    
    override func viewDidLoad() {
        super.viewDidLoad();
        // Start animation when comes to the forground
        NotificationCenter.default.addObserver(self, selector: #selector(GamePrepViewController.enterForeground(_:)), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil);
        
        // Background color
        //self.view.backgroundColor = UIColor(red: 0.93, green:0.93, blue:0.93, alpha:1);
        
        // Background image view
        backgroundImageView = UIImageView();
        backgroundImageView.contentMode = UIViewContentMode.scaleAspectFill;
        backgroundImageView.layer.zPosition = -1;
        self.view.addSubview(backgroundImageView);
        
        // View title label
        viewTitleLabel.font = UIFont(name: "Kenvector Future", size: 20);
        
        // Player Title label
        player1TitleLabel.font = UIFont(name: "Kenvector Future", size: 15);
        player1TitleLabel.textColor = GameConstants.RED_COLOR
        player2TitleLabel.font = UIFont(name: "Kenvector Future", size: 15);
        player2TitleLabel.textColor = GameConstants.BLUE_COLOR

        // VS label
        vsLabel.font = UIFont(name: "Kenvector Future", size: 20);
        vsLabel.textColor = GameConstants.GOLD_COLOR
        
        // Note label
        IconSelectionNoteLabel.font = UIFont(name: "Kenvector Future Thin", size: 10);
        IconSelectionNoteLabel.textColor = GameConstants.BLACK_COLOR
        
        // Player Info
        player1IconNameLabel.font = UIFont(name: "Kenvector Future Thin", size: 10);
        player1IconNameLabel.textColor = GameConstants.DARKGREY_COLOR
        player2IconNameLabel.font = UIFont(name: "Kenvector Future Thin", size: 10);
        player2IconNameLabel.textColor = GameConstants.DARKGREY_COLOR
        refreshPlayerInfo()
        
        // Player 1 label
        player1Label.font = UIFont(name: "Kenvector Future Thin", size: 17);
        player1Label.textAlignment = NSTextAlignment.left;
        
        // Player 1 selector
        player1Selector.setBackgroundImage(UIImage(named: "yellow_button03"), for: UIControlState(), barMetrics: UIBarMetrics.default);
        player1Selector.setBackgroundImage(UIImage(named: "red_button07"), for: UIControlState.selected, barMetrics: UIBarMetrics.default);
        player1Selector.setTitleTextAttributes([NSAttributedStringKey.font: UIFont(name: "Kenvector Future Thin", size: 9)!], for: UIControlState());
        player1Selector.setTitleTextAttributes([NSAttributedStringKey.font: UIFont(name: "Kenvector Future", size: 9)!], for: UIControlState.selected);
        
        // Player 2 label
        player2Label.font = UIFont(name: "Kenvector Future Thin", size: 17);
        player2Label.textAlignment = NSTextAlignment.left;
        
        // Game board size title label
        boardSizeTitleLabel.font = UIFont(name: "Kenvector Future Thin", size: 17);
        boardSizeTitleLabel.textAlignment = NSTextAlignment.left;
        player2Selector.setTitleTextAttributes([NSAttributedStringKey.font: UIFont(name: "Kenvector Future Thin", size: 9)!], for: UIControlState());
        player2Selector.setTitleTextAttributes([NSAttributedStringKey.font: UIFont(name: "Kenvector Future", size: 9)!], for: UIControlState.selected);
        
        // Player 2 selector
        player2Selector.setBackgroundImage(UIImage(named: "yellow_button03"), for: UIControlState(), barMetrics: UIBarMetrics.default);
        player2Selector.setBackgroundImage(UIImage(named: "blue_button10"), for: UIControlState.selected, barMetrics: UIBarMetrics.default);
        
        // Game board size label
        boardSizeLabel.font = UIFont(name: "Kenvector Future Thin", size: 17);
        boardSizeLabel.textAlignment = NSTextAlignment.center;
        let sizeValue:Int = Int(gameBoardSlider.value);
        boardSizeLabel.text = "\(sizeValue) ✕ \(sizeValue)";
        
        // Game board size slider
        gameBoardSlider.setThumbImage(UIImage(named: "green_sliderDown"), for: UIControlState());
        gameBoardSlider.setMaximumTrackImage(UIImage(named: "grey_sliderHorizontal"), for: UIControlState());
        gameBoardSlider.setMinimumTrackImage(UIImage(named: "grey_sliderHorizontal"), for: UIControlState());
        gameBoardSlider.minimumValueImage = nil;
        gameBoardSlider.maximumValueImage = nil;
        
        // Start game button
        startGameButton.titleLabel?.font = UIFont(name: "Kenvector Future", size: 20);
        startGameButton.setBackgroundImage(UIImage(named: "green_button00"), for: UIControlState())
        startGameButton.setBackgroundImage(UIImage(named: "green_button01"), for: UIControlState.highlighted)
        
        // Back button
        backButton.titleLabel?.font = UIFont(name: "Kenvector Future", size: 20);
        backButton.setBackgroundImage(UIImage(named: "red_button01"), for: UIControlState())
        backButton.setBackgroundImage(UIImage(named: "red_button02"), for: UIControlState.highlighted)
        
    }
    
    override func viewWillAppear(_ animated:Bool) {
        super.viewWillAppear(animated);
        
        startAnimation();
    }
    
    @IBAction func gameBoardSizeChanged(_ sender: UISlider) {
        let sizeValue:Int = Int(sender.value);
        boardSizeLabel.text = "\(sizeValue) ✕ \(sizeValue)";
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any!) {
        if (segue.identifier == "PlayGame") {
            let gameViewController = segue.destination as! GameViewController;
            gameViewController.boardSize = Int(gameBoardSlider.value);
            
            gameViewController.gamePlayer1 = PlayerFactory.getOfflinePlayer(iconIndex: player1IconIndex, role: Role.p1, playerType: playerAvailable[player1Selector.selectedSegmentIndex])
            gameViewController.gamePlayer2 = PlayerFactory.getOfflinePlayer(iconIndex: player2IconIndex, role: Role.p2, playerType: playerAvailable[player2Selector.selectedSegmentIndex])
        }
    }
    
    @IBAction func clickedButton(_ sender: UIButton) {
        AudioUtils.playButtonClickSound();
        // Back button clicked
        if (sender.restorationIdentifier == "BackButton") {
            self.dismiss(animated: true, completion: nil);
        }
    }
    
    @IBAction func clickedIconSelectButton(_ sender: UIButton) {
        AudioUtils.playButtonClickSound();
        // Select button clicked
        if (sender.restorationIdentifier == "Player1Select") {
            player1IconIndex = PlayerIconUtils.getNextIconIndex(player1IconIndex)
        } else if (sender.restorationIdentifier == "Player2Select") {
            player2IconIndex = PlayerIconUtils.getNextIconIndex(player2IconIndex)
        }
        refreshPlayerInfo()
    }
    
    
    @objc fileprivate func enterForeground(_ notification: Notification) {
        startAnimation();
    }
    
    fileprivate func refreshPlayerInfo() {
        player1IconNameLabel.text = PlayerIconUtils.getIconName(player1IconIndex)
        player2IconNameLabel.text = PlayerIconUtils.getIconName(player2IconIndex)
        player1IconButton.setBackgroundImage(PlayerIconUtils.getImageOfIcon(player1IconIndex), for: UIControlState.normal)
        player2IconButton.setBackgroundImage(PlayerIconUtils.getImageOfIcon(player2IconIndex), for: UIControlState.normal)
    }
    
    // Load the current selected player icon.
    fileprivate func startAnimation() {
        // Init animated object satus
        initBackgroundImage();
        
        // Move the bg image horizontally
        UIView.animate(withDuration: 10, delay: 0, options: [UIViewAnimationOptions.repeat, UIViewAnimationOptions.autoreverse] , animations: {
            self.backgroundImageView.frame = CGRect(
                origin: CGPoint(x: -self.moveXOffset, y: 0),
                size: self.backgroundImageView.frame.size);
            },
            completion: nil);
    }
    
    fileprivate func initBackgroundImage() {
        // Background image
        let backgroundImage:UIImage = GameUtils.getRandomBackgroundUncoloredImage()!;
        
        // Scale image
        let scaledImage:UIImage = GameUtils.scaleImageToFitHeight(backgroundImage, targetHeight: self.view.bounds.size.height);
        backgroundImageView.image = scaledImage;
        moveXOffset = abs(scaledImage.size.width-self.view.frame.size.width)/2;
        backgroundImageView.frame = CGRect(origin: CGPoint(x: moveXOffset, y: 0), size: self.view.frame.size);
    }
}
