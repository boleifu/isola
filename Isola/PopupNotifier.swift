//
//  PopupNotifier.swift
//  Isola
//
//  Created by Bolei Fu on 10/14/15.
//  Copyright © 2015 boleifu. All rights reserved.
//

import SpriteKit

class PopupNotifier : SKSpriteNode {
    
    let titleLabel:SKLabelNode
    let detailLabel1:SKLabelNode
    let detailLabel2:SKLabelNode
    let detailLabel3:SKLabelNode

    // Constructor
    init(size:CGSize) {
        self.titleLabel = SKLabelNode(fontNamed: "KenVector Future Regular");
        self.detailLabel1 = SKLabelNode(fontNamed: "KenVector Future Regular");
        self.detailLabel2 = SKLabelNode(fontNamed: "KenVector Future Regular");
        self.detailLabel3 = SKLabelNode(fontNamed: "KenVector Future Regular");
        super.init(texture: nil, color: SKColor.white, size: size);
        
        // Set up the game board
        self.name = GameConstants.POPUPNOTIFIER_NODE_NAME;
        self.texture?.filteringMode = SKTextureFilteringMode.nearest;
        self.color = GameConstants.GOLD_COLOR;
        self.zPosition = CGFloat(LayerDepth.popupnotifier.rawValue);
        self.alpha = 0.8;

        titleLabel.text = "You WIN"
        titleLabel.position = CGPoint(x: 0, y: size.height/5*2);
        titleLabel.fontColor = GameConstants.BLACK_COLOR
        titleLabel.fontSize = 30;
        titleLabel.zPosition = CGFloat(LayerDepth.other.rawValue);
        titleLabel.verticalAlignmentMode = SKLabelVerticalAlignmentMode.top;
        titleLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.center;
        self.addChild(titleLabel);

        detailLabel1.text = "Score: 100 + 10"
        detailLabel1.position = CGPoint(x: -size.width/3, y: size.height/8);
        detailLabel1.fontColor = GameConstants.BLACK_COLOR
        detailLabel1.fontSize = 18;
        detailLabel1.zPosition = CGFloat(LayerDepth.other.rawValue);
        detailLabel1.verticalAlignmentMode = SKLabelVerticalAlignmentMode.top;
        detailLabel1.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.left;
        self.addChild(detailLabel1);

        detailLabel2.text = "Win: 34 + 1"
        detailLabel2.position = CGPoint(x: -size.width/3, y: 0);
        detailLabel2.fontColor = GameConstants.BLACK_COLOR
        detailLabel2.fontSize = 18;
        detailLabel2.zPosition = CGFloat(LayerDepth.other.rawValue);
        detailLabel2.verticalAlignmentMode = SKLabelVerticalAlignmentMode.top;
        detailLabel2.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.left;
        self.addChild(detailLabel2);

        detailLabel3.text = "Lose: 10 + 0"
        detailLabel3.position = CGPoint(x: -size.width/3, y: -size.height/8);
        detailLabel3.fontColor = GameConstants.BLACK_COLOR
        detailLabel3.fontSize = 18;
        detailLabel3.zPosition = CGFloat(LayerDepth.other.rawValue);
        detailLabel3.verticalAlignmentMode = SKLabelVerticalAlignmentMode.top;
        detailLabel3.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.left;
        self.addChild(detailLabel3);
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

