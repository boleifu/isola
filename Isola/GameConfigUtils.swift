//
//  GameConfigUtils.swift
//  Isola
//
//  Created by Bolei Fu on 10/17/15.
//  Copyright © 2015 boleifu. All rights reserved.
//

import Foundation
import UIKit

class GameConfigUtils {
    
    static var configDict:NSDictionary? = nil;
    
    // Game configuration
    // Get the config file path from the app bundle
    static func getConfigFileBundlePath() -> String? {
        let bundlePath = Bundle.main.path(forResource: GameConstants.CONFIG_FILE_NAME, ofType:GameConstants.CONFIG_FILE_TYPE);
        print("Config file bundle path = \(String(describing: bundlePath))");
        return bundlePath;
    }
    
    // Get the config file path from the user domain
    static func getConfigFilePath() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true) as NSArray
        let documentsDirectory = paths.object(at: 0) as! NSString
        let path = documentsDirectory.appendingPathComponent(GameConstants.CONFIG_FILE_FULL_NAME);
        print("Config file path = \(path)");
        return path;
    }
    
    // Read the config file and get the config details.
    // Plist files in the project bundle are read only.
    // You need to save a copy of the plist in the Documents folder than edit and read that one.
    static func getConfigDict(forceRefresh:Bool=false) -> NSDictionary? {
        if (GameConfigUtils.configDict == nil || forceRefresh) {
            let path = GameConfigUtils.getConfigFilePath();
            let fileManager = FileManager.default;
            if (!fileManager.fileExists(atPath: path)) {
                // Config file not in the user domain, copy from the bundle
                let bundlePath:String? = GameConfigUtils.getConfigFileBundlePath();
                if (bundlePath == nil) {
                    print("Error: Cannot get game configurations from bundle.");
                    return nil;
                }
                print("Config file not in the user domain, copy from the bundle path = \(bundlePath!).")
                do {
                    try fileManager.copyItem(atPath: bundlePath!, toPath: path);
                } catch {
                    print("Copying the config file from bundle to user domain failed. Error:\(error)");
                    return nil;
                }
            }
            
            GameConfigUtils.configDict = NSDictionary(contentsOfFile: path);
        }
        //print("Current game config: \(GameConfigUtils.configDict)")
        return GameConfigUtils.configDict;
    }
    
    static func isTestMode() -> Bool {
        let currentConfig: NSDictionary? = GameConfigUtils.getConfigDict();
        if (currentConfig != nil) {
            let isTestMode:Bool? = currentConfig!["TestMode"] as? Bool;
            if (isTestMode != nil) {
                return isTestMode!;
            }
        }
        print("Error: Cannot get TestMode configuration.");
        return false;
    }
    
    static func isClickSoundEnabled() -> Bool {
        let currentConfig: NSDictionary? = GameConfigUtils.getConfigDict();
        if (currentConfig != nil) {
            let clickSoundEnabled:Bool? = currentConfig!["ClickSound"] as? Bool;
            if (clickSoundEnabled != nil) {
                return clickSoundEnabled!;
            }
        }
        print("Error: Cannot get ClickSound configuration.");
        return false;
    }
    
    static func isSFXEnabled() -> Bool {
        let currentConfig: NSDictionary? = GameConfigUtils.getConfigDict();
        if (currentConfig != nil) {
            let sfxEnabled:Bool? = currentConfig!["GameSFX"] as? Bool;
            if (sfxEnabled != nil) {
                return sfxEnabled!;
            }
        }
        print("Error: Cannot get Game SFX configuration.");
        return false;
    }
    
    static func isBGMEnabled() -> Bool {
        let currentConfig: NSDictionary? = GameConfigUtils.getConfigDict();
        if (currentConfig != nil) {
            let sfxEnabled:Bool? = currentConfig!["BGM"] as? Bool;
            if (sfxEnabled != nil) {
                return sfxEnabled!;
            }
        }
        print("Error: Cannot get Game SFX configuration.");
        return false;
    }
    
    static func isMoveHintEnabled() -> Bool {
        let currentConfig: NSDictionary? = GameConfigUtils.getConfigDict();
        if (currentConfig != nil) {
            let moveHintEnabled:Bool? = currentConfig!["MoveHint"] as? Bool;
            if (moveHintEnabled != nil) {
                return moveHintEnabled!;
            }
        }
        print("Error: Cannot get MoveHint configuration.");
        return false;
    }
    
    // Persist the config file with the given configuration
    // Plist files in the project bundle are read only.
    // You need to save a copy of the plist in the Documents folder than edit and read that one.
    static func updateGameConfig(clickSoundEnabled:Bool, gameSFXEnabled:Bool, gameBGMEnabled:Bool, moveHintEnabled:Bool) {
        let path:String? = GameConfigUtils.getConfigFilePath();
        if (path == nil) {
            print("Error: Cannot get config file path.");
            return;
        }
        
        let existingConfig: NSDictionary? = GameConfigUtils.getConfigDict();
        if (existingConfig == nil) {
            return;
        }
        
        let newConfig:NSMutableDictionary = NSMutableDictionary(dictionary: existingConfig!);
        newConfig.setObject(clickSoundEnabled, forKey: "ClickSound" as NSCopying);
        newConfig.setObject(gameSFXEnabled, forKey: "GameSFX" as NSCopying);
        newConfig.setObject(gameBGMEnabled, forKey: "BGM" as NSCopying);
        newConfig.setObject(moveHintEnabled, forKey: "MoveHint" as NSCopying);
        
        let succeed:Bool = newConfig.write(toFile: path!, atomically:false);
        
        print("Config file updated. Path = \(path!). Succeed = \(succeed)");
        // Refresh the cached config
        _ = getConfigDict(forceRefresh:true);
    }

}
