//
//  IntroViewController.swift
//  Isola
//
//  Created by Bolei Fu on 10/12/15.
//  Copyright © 2015 boleifu. All rights reserved.
//

import UIKit
import GameKit

class IntroViewController: UIViewController, GKGameCenterControllerDelegate {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var additionalTitleLabel: UILabel!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var onlinePlayButton: UIButton!
    @IBOutlet weak var howToPlayButton: UIButton!
    @IBOutlet weak var settingButton: UIButton!
    @IBOutlet weak var creditButton: UIButton!
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var icon1Image: UIImageView!
    @IBOutlet weak var icon2Image: UIImageView!
    
    let iconRotateAngle:CGFloat = CGFloat(Double.pi/180*30);
    var backgroundImageView:UIImageView = UIImageView();
    var moveXOffset:CGFloat = 0;
    
    deinit {
        NotificationCenter.default.removeObserver(self);
        print("IntroViewController is deinited.");
    }
    
    override func viewDidLoad() {
        super.viewDidLoad();

        // Login Game Center
        authenticateLocalPlayer()
        
        // Start animation when comes to the forground
        NotificationCenter.default.addObserver(self, selector: #selector(IntroViewController.enterForeground(_:)), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil);
        
        // Background color
        self.view.backgroundColor = UIColor.white;
        
        // Background image view
        backgroundImageView = UIImageView();
        backgroundImageView.contentMode = UIViewContentMode.scaleAspectFill;
        backgroundImageView.layer.zPosition = -1;
        self.view.addSubview(backgroundImageView);
        
        // Title
        titleLabel.font = UIFont(name: "ChalkboardSE-Bold", size: 45);
        titleLabel.textAlignment = NSTextAlignment.center;
        titleLabel.textColor = GameConstants.BLACK_COLOR;
        additionalTitleLabel.font = UIFont(name: "ChalkboardSE-Bold", size: 45);
        additionalTitleLabel.textAlignment = NSTextAlignment.center;
        additionalTitleLabel.textColor = GameConstants.BLACK_COLOR;
        
        // Animal Icons
        self.icon1Image.transform = CGAffineTransform(rotationAngle: -iconRotateAngle);
        self.icon2Image.transform = CGAffineTransform(rotationAngle: -iconRotateAngle);
        
        // Version
        versionLabel.font = UIFont(name: "Kenvector Future Thin", size: 10);
        versionLabel.text = GameUtils.version();
        
        // Play Button
        playButton.titleLabel?.font = UIFont(name: "Kenvector Future", size: 25);
        playButton.setBackgroundImage(UIImage(named: "yellow_button00"), for: UIControlState())
        playButton.setBackgroundImage(UIImage(named: "yellow_button01"), for: UIControlState.highlighted)
        
        // Online Play Button
        onlinePlayButton.titleLabel?.font = UIFont(name: "Kenvector Future", size: 25);
        onlinePlayButton.setBackgroundImage(UIImage(named: "yellow_button00"), for: UIControlState())
        onlinePlayButton.setBackgroundImage(UIImage(named: "yellow_button01"), for: UIControlState.highlighted)
        
        // HowToPlay Button
        howToPlayButton.titleLabel?.font = UIFont(name: "Kenvector Future", size: 20);
        howToPlayButton.setBackgroundImage(UIImage(named: "blue_button00"), for: UIControlState())
        howToPlayButton.setBackgroundImage(UIImage(named: "blue_button01"), for: UIControlState.highlighted)
        
        // Setting Button
        settingButton.titleLabel?.font = UIFont(name: "Kenvector Future", size: 20);
        settingButton.setBackgroundImage(UIImage(named: "blue_button00"), for: UIControlState())
        settingButton.setBackgroundImage(UIImage(named: "blue_button01"), for: UIControlState.highlighted)
        
        // Credit Button
        creditButton.titleLabel?.font = UIFont(name: "Kenvector Future", size: 20);
        creditButton.setBackgroundImage(UIImage(named: "blue_button00"), for: UIControlState())
        creditButton.setBackgroundImage(UIImage(named: "blue_button01"), for: UIControlState.highlighted)
        
        // Setup audio players
        AudioUtils.setupAllAudioPlayers()
        
        // Start BGM
        AudioUtils.playMainBgm();
    }
    
    override func viewWillAppear(_ animated:Bool) {
        super.viewWillAppear(animated);
        
        startAnimation();
        //print("IntroViewController will appear.")
    }
    
    @objc fileprivate func enterForeground(_ notification: Notification) {
        startAnimation();
    }
    
    fileprivate func startAnimation() {
        // Init animated object satus
        initBackgroundImage();
        self.icon1Image.transform = CGAffineTransform(rotationAngle: -iconRotateAngle);
        self.icon2Image.transform = CGAffineTransform(rotationAngle: -iconRotateAngle);
        
        // Move the bg image horizontally
        UIView.animate(withDuration: 10, delay: 0, options: [UIViewAnimationOptions.repeat, UIViewAnimationOptions.autoreverse] , animations: {
            self.backgroundImageView.frame = CGRect(
                origin: CGPoint(x: -self.moveXOffset, y: 0),
                size: self.backgroundImageView.frame.size);
            },
            completion: nil);
        
        // Make the animal icon rotate
        UIView.animate(withDuration: 1, delay: 0, options: [UIViewAnimationOptions.repeat, UIViewAnimationOptions.autoreverse] , animations: {
            self.icon1Image.transform = CGAffineTransform(rotationAngle: self.iconRotateAngle);
            self.icon2Image.transform = CGAffineTransform(rotationAngle: self.iconRotateAngle);
            },
            completion: nil);
    }
    
    fileprivate func initBackgroundImage() {
        // Background image
        let backgroundImage:UIImage = GameUtils.getRandomBackgroundImage()!;
        
        // Scale image
        let scaledImage:UIImage = GameUtils.scaleImageToFitHeight(backgroundImage, targetHeight: self.view.bounds.size.height);
        backgroundImageView.image = scaledImage;
        moveXOffset = abs(scaledImage.size.width-self.view.frame.size.width)/2;
        backgroundImageView.frame = CGRect(origin: CGPoint(x: moveXOffset, y: 0), size: self.view.frame.size);
    }
    
    @IBAction func clickedButton(_ sender: UIButton) {
        AudioUtils.playButtonClickSound();
    }

    // Game Center
    func authenticateLocalPlayer() {
        let localPlayer = GKLocalPlayer.localPlayer()
        localPlayer.authenticateHandler = {(viewController, error) -> Void in
            if (viewController != nil) {
                // Login
                print("Presenting login page.")
                self.present(viewController!, animated: true, completion: nil)
            } else if (localPlayer.isAuthenticated) {
                print("Player authorized.")
            } else {
                print("Player not authorized. Error:\(String(describing: error))")
                //self.dismiss(animated: true, completion: nil);
            }
        }
    }
    
    func gameCenterViewControllerDidFinish(_ gameCenterViewController: GKGameCenterViewController)
    {
        gameCenterViewController.dismiss(animated: true, completion: nil)
    }
}
