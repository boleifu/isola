//
//  BoardUtils.swift
//  Isola
//
//  Created by Bolei Fu on 10/7/15.
//  Copyright © 2015 boleifu. All rights reserved.
//

import SpriteKit

//
// The util class contains some help functions related to the game board
//
class BoardUtils {
    
    // Convert the grid coordinate to the position in the GameBoard
    static func getGridPosition(_ col:Int, _ row:Int, gridWidth: CGFloat) -> CGPoint {
        if (col < 0 || row < 0) {
            print("Error: Invalid coordinate [\(col),\(row)]");
            return GameConstants.INVALID_POSITION;
        }
        
        let xPos:CGFloat = gridWidth * CGFloat(col);
        let yPos:CGFloat = gridWidth * CGFloat(row);
        let outputPoint:CGPoint = CGPoint(x: xPos, y: yPos);
        //print("[\(col),\(row)] ==> [\(outputPoint)]");
        return outputPoint;
    }
    
    // Convert a position on the GameBoard to corresponding grid coordinate
    static func getGridCoordinate(_ posOnBoard:CGPoint, gridSize:CGSize) -> GridCoordinate {
        if (posOnBoard.x < 0 || posOnBoard.y < 0) {
            print("Error: Invalid position [\(posOnBoard)]");
            return GameConstants.INVALID_COORDINATE;
        }
        
        let col:Int = Int(posOnBoard.x / gridSize.width);
        let row:Int = Int(posOnBoard.y / gridSize.height);
        let outputCoordinate:GridCoordinate = GridCoordinate(col:col, row:row)
        //print("[\(posOnBoard)] ==> " + outputCoordinate.toString());
        return outputCoordinate;
    }
    
    // Check whether two grid is adjacent
    static func isAdjacent(_ grid1:GridCoordinate, _ grid2:GridCoordinate) -> Bool {
        if (grid1.col == grid2.col && grid1.row == grid2.row) {
            return false;
        }
        return abs(grid1.col-grid2.col) <= 1 && abs(grid1.row-grid2.row) <= 1;
    }
    
    static func isInDistance(_ grid1:GridCoordinate, _ grid2:GridCoordinate, distance:Int) -> Bool {
        return abs(grid1.col-grid2.col) <= distance && abs(grid1.row-grid2.row) <= distance;
    }
}
