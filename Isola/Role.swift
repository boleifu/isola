//
//  Role.swift
//  Isola
//
//  Created by Bolei Fu on 10/10/15.
//  Copyright © 2015 boleifu. All rights reserved.
//

enum Role {
    case p1;
    case p2;
    
    func getOpponent() -> Role {
        if (self == Role.p1) {
            return Role.p2;
        } else {
            return Role.p1;
        }
    }
}
