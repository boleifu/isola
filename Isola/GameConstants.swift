//
//  GameConstants.swift
//  Isola
//
//  Created by Bolei Fu on 10/7/15.
//  Copyright © 2015 boleifu. All rights reserved.
//

import SpriteKit

//
// This class contains the constants used in the game
//
class GameConstants {
    
    // Node name
    static let GAMEBOARD_NODE_NAME = "GameBoard";
    static let BOARDGRID_NODE_NAME = "BoardGrid";
    static let HUDBOARD_NODE_NAME = "HudBoard";
    static let BOTTOM_HUDBOARD_NODE_NAME = "BottomHudBoard"
    static let POPUPNOTIFIER_NODE_NAME = "PopupNotifier";
    
    // Invalid Value
    static let INVALID_COORDINATE:GridCoordinate = GridCoordinate(col:-1, row:-1);
    static let INVALID_POSITION:CGPoint = CGPoint(x: -1, y: -1);
    
    // Message
    static let INVALID_MOVE:String = "This move is not allowd";
    static let NO_MESSAGE:String = "";
    
    // File Path
    static let CONFIG_FILE_NAME:String = "Config";
    static let CONFIG_FILE_TYPE:String = "plist";
    static let CONFIG_FILE_FULL_NAME:String = "Config.plist";
    
    // Player Category
    static let HUMANPLAYER:String = "Human";
    static let EASY_AI:String = "EASY_AI";
    static let HARD_AI:String = "HARD_AI";
    
    // Author Info
    static let AUTHOR_EMAIL:String = "bolei.game@gmail.com";
    
    // Audio files
    static let BUTTON_CLICK_SOUND_FILE_NAME = "click1";
    static let ERROR_CLICK_SOUND_FILE_NAME = "error"
    
    static let MAIN_THEME_BGM_FILE_NAME = "polon";
    static let GAME_BGM_FILE_NAME = "konekone_cut";
    
    static let WAV_FILE_TYPE = "wav";
    static let MP3_FILE_TYPE = "mp3";
    
    static let DEFAULT_BGM_VOLUMN:Float = 0.5;
    static let DEFAULT_SE_VOLUMN:Float = 1.0;
    
    // Game Center
    static let SCORE_LEADER_BOARD_ID = "grp.battle_points"
    static let WIN_LEADER_BOARD_ID = "grp.win_num"
    static let LOSE_LEADER_BOARD_ID = "grp.lose_num"
    
    // Colors
    static var BLACK_COLOR = UIColor(red:0.12, green:0.12, blue:0.13, alpha:1.0); // #1F1F21
    static var GOLD_COLOR = UIColor(red:1.00, green:0.80, blue:0.00, alpha:1.0);  // #FFCC00
    static var RED_COLOR = UIColor(red:1.00, green:0.07, blue:0.00, alpha:1.0);   // #FF1300
    static var GREEN_COLOR = UIColor(red:0.30, green:0.85, blue:0.39, alpha:1.0); // #4CD964
    static var BLUE_COLOR = UIColor(red:0.00, green:0.48, blue:1.00, alpha:1.0);  //#007AFF
    static var SKYBLUE_COLOR = UIColor(red:0.82, green:0.93, blue:0.99, alpha:1.0); //#D1EEFC
    static var DARKGREY_COLOR = UIColor(red:0.56, green:0.56, blue:0.58, alpha:1.0); //#8E8E93
    static var SILVER_COLOR = UIColor(red:0.89, green:0.87, blue:0.79, alpha:1.0); //#E4DDCA
    
    // Pre-defined action
    static let FADE_AWAY = SKAction.sequence([SKAction.fadeAlpha(to: 0, duration: 1), SKAction.removeFromParent()]);
    static let REMOVE_AFTER_1_SEC = SKAction.sequence([SKAction.wait(forDuration: 1), SKAction.removeFromParent()]);
    static let SHOW_1_SEC = SKAction.sequence([SKAction.unhide(), SKAction.wait(forDuration: 1), SKAction.hide()]);
    static let BLINK = SKAction.sequence([SKAction.fadeOut(withDuration: 0.1), SKAction.fadeIn(withDuration: 0.1)]);
    static let BLINK_FOR_TIME = SKAction.repeat(BLINK, count: 10);
    static let SHAKE = SKAction.sequence([SKAction.rotate(byAngle: -CGFloat(Double.pi/4), duration: 0.1), SKAction.rotate(byAngle: CGFloat(Double.pi/2), duration: 0.2), SKAction.rotate(byAngle: -CGFloat(Double.pi/4), duration: 0.1)]);
    static let SMALL_SHAKE = SKAction.sequence([SKAction.rotate(byAngle: -CGFloat(Double.pi/4/2), duration: 1), SKAction.rotate(byAngle: CGFloat(Double.pi/4), duration: 2), SKAction.rotate(byAngle: -CGFloat(Double.pi/4/2), duration: 1)]);
    static let SCALE_BLINK = SKAction.sequence([SKAction.scale(to: 0.5, duration: 0.5), SKAction.scale(to: 1.5, duration: 0.5)]);
    static let HILIGHT = SKAction.repeat(SCALE_BLINK, count: 3);
    static let POP_UP = SKAction.sequence([SKAction.scale(to: 0, duration: 0), SKAction.scale(to: 1, duration: 0.5)]);
    static let VERTICAL_POP_UP = SKAction.sequence([SKAction.scaleY(to: 0, duration: 0), SKAction.scaleY(to: 1, duration: 0.5)]);
    
    // Online Mode
    static let GAME_TURN_TIME:TimeInterval = 15
}
