//
//  GameScene.swift
//  Isola
//
//  Created by Bolei Fu on 10/6/15.
//  Copyright (c) 2015 boleifu. All rights reserved.
//

import SpriteKit
import Foundation
import GameKit

//
// This is the main game scene
//
class GameScene: SKScene {
    // Run time properties
    var touchDisabled:Bool = false;
    var lastUpdatedTime:TimeInterval = -1
    var gameEnabled = true
    
    // Game Scene content
    var hudBoard:HudBoard? = nil;
    var bottomHudBoard:BottomHudBoard? = nil;
    var gameBoard:GameBoard? = nil;
    var popOutNotifier:PopupNotifier? = nil;
    var gameCore:GameCore? = nil;
    
    // Game properties
    var boardSize:Int = 5;
    var theme:Theme? = nil;
    
    var gamePlayer1:GamePlayer? = nil;
    var gamePlayer2:GamePlayer? = nil;
    
    // Online game mode
    var isOnlineMode:Bool = false
    weak var gkMatch:GKMatch? = nil
    var opponentPlay:GridCoordinate? = nil
    var currentTurnNum:Int = 0
    var currentTurnTimeRemains:TimeInterval = GameConstants.GAME_TURN_TIME

    deinit {
        gameBoard?.cleanUpBoard();
        gameBoard = nil;
        self.removeAllActions();
        self.removeAllChildren();
        print("GameScene is deinited.")
    }
  
    override func didMove(to view: SKView) {
        
        self.anchorPoint = CGPoint(x: 0.5, y: 0.5);
        var gameBoardWidth:CGFloat = min(self.size.width, self.size.height);
        
        // Set up theme
        if (theme == nil) {
            theme = GameUtils.getRandomeTheme();
        }
        
        // Setup back ground
        // Bottom back ground
        let topBackGround:SKSpriteNode = SKSpriteNode(imageNamed: theme!.topBackGroundImageName);
        topBackGround.size = CGSize(width: gameBoardWidth, height: (self.size.height-gameBoardWidth)/2);
        topBackGround.position = CGPoint(x: 0, y: -(gameBoardWidth+self.size.height)/4);
        topBackGround.zPosition = CGFloat(LayerDepth.background.rawValue);
        topBackGround.texture!.filteringMode = SKTextureFilteringMode.nearest;
        self.addChild(topBackGround);
        
        // Top back ground
        let bottomBackGround:SKSpriteNode = SKSpriteNode(imageNamed: theme!.bottomBackGroundImageName);
        bottomBackGround.size = CGSize(width: gameBoardWidth, height: (self.size.height-gameBoardWidth)/2);
        bottomBackGround.position = CGPoint(x: 0, y: (gameBoardWidth+self.size.height)/4);
        bottomBackGround.zPosition = CGFloat(LayerDepth.background.rawValue);
        bottomBackGround.texture!.filteringMode = SKTextureFilteringMode.nearest;
        self.addChild(bottomBackGround);
        
        // Setup game board layer
        gameBoardWidth = gameBoardWidth - (gameBoardWidth.truncatingRemainder(dividingBy: CGFloat(boardSize)));
        gameBoard = GameBoard(size: CGSize(width: gameBoardWidth, height: gameBoardWidth), boardSize: BoardSize(col: boardSize, row: boardSize), gridNum:boardSize, theme: theme!, player1IconIndex: gamePlayer1!.playerIconIndex, player2IconIndex: gamePlayer2!.playerIconIndex);
        
        // Set up the node properties
        gameBoard!.position = CGPoint(x: -gameBoardWidth/2, y: -gameBoardWidth/2);
        gameBoard!.zPosition = CGFloat(LayerDepth.gameboard.rawValue);
        gameBoard!.anchorPoint = CGPoint.zero;
        self.addChild(gameBoard!);
        
        // Setup HUD board layer
        hudBoard = HudBoard(size: CGSize(width: gameBoardWidth, height: (self.size.height-gameBoardWidth)/2));
        hudBoard!.zPosition = CGFloat(LayerDepth.hudboard.rawValue);
        hudBoard!.anchorPoint = CGPoint(x: 0.5, y: 1);
        hudBoard!.position = CGPoint(x: 0, y: -gameBoardWidth/2);
        self.addChild(hudBoard!);
        
        // Setup Bottom HUD board layer
        bottomHudBoard = BottomHudBoard(size: CGSize(width: gameBoardWidth, height: (self.size.height-gameBoardWidth)/2), player1: gamePlayer1!, player2: gamePlayer2!, onlineMode:isOnlineMode);
        bottomHudBoard!.zPosition = CGFloat(LayerDepth.hudboard.rawValue);
        bottomHudBoard!.anchorPoint = CGPoint(x: 0.5, y: 1);
        bottomHudBoard!.position = CGPoint(x: 0, y: gameBoardWidth/2+bottomHudBoard!.size.height);
        self.addChild(bottomHudBoard!);
        
        // Setup pop out notifier
        popOutNotifier = PopupNotifier(size: CGSize(width: gameBoardWidth/4*3, height: gameBoardWidth/4*3));
        popOutNotifier!.anchorPoint = CGPoint(x: 0.5, y: 0.5);
        popOutNotifier!.position = CGPoint(x: 0, y: 0);
        self.addChild(popOutNotifier!);
        popOutNotifier!.isHidden = true

        print("RootLayer size = \(self.size)");
        print("GameBoardLayer size = \(gameBoard!.size), pos = \(gameBoard!.position)");
        print("HudBoardLayer size = \(hudBoard!.size), pos = \(hudBoard!.position)");
        print("BottomHudBoardLayer size = \(bottomHudBoard!.size), pos = \(bottomHudBoard!.position)");
        
        // Set up GameCore
        gameCore = GameCore(gameBoard:gameBoard!, hudBoard:hudBoard!, playerStatusHUDBoard: bottomHudBoard!, player1: gamePlayer1!, player2: gamePlayer2!, theme: theme!, onlineMode:isOnlineMode);
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if (touchDisabled) {
            print("Touch disabled...");
            return;
        }
        
       /* Called when a touch begins */
        let touch:UITouch = touches.first!;
        let touchPos:CGPoint = touch.location(in: gameBoard!);
        
        if (gameBoard!.isInTheBoard(touchPos)) {
            let targetGrid:GridCoordinate = BoardUtils.getGridCoordinate(touchPos, gridSize: gameBoard!.gridSize);
            let gameTurnProcessed = gameCore!.updateByHuman(targetGrid);
            
            if (gameTurnProcessed && isOnlineMode) {
                // Send game turn message to opponent.
                sendGameTurnPacket(targetGrid: targetGrid)
                startNewTurn()
            }
        } else {
            print("Out of the game board!!!");
        }
    }
   
    override func update(_ currentTime: TimeInterval) {
        if lastUpdatedTime < 0 {
            // Skip the first frame
            lastUpdatedTime = currentTime
            return
        }
        
        let deltaTime = currentTime - lastUpdatedTime
        lastUpdatedTime = currentTime
        
        if !gameEnabled || gameCore == nil{
            return
        }
        
        /* Called before each frame is rendered */
        if !isOnlineMode {
            gameCore!.processAiTurn();
            //print("SpriteKit frame updated. \(currentTime)");
        } else {
            // Check game result
            let gameResult = gameCore!.checkOnlineGameResult()
            if  gameResult != 0 {
                print("Online game over.")
                onelineGameOver(gameResult > 0)
                return
            }
            
            currentTurnTimeRemains -= deltaTime
            bottomHudBoard?.updateCountDown(currentTurnTimeRemains)
            
            // Check turn timeout
            if currentTurnTimeRemains < 0 {
                print("Game turn timed out.")
                //AudioUtils.playGameOverSound()
                onelineGameOver(!gameCore!.isSelfTurn())
                return
            }
            
            if opponentPlay != nil {
                let playAccepted = gameCore!.processOpponentPlay(input: opponentPlay!)
                if playAccepted {
                    print("Opponent's play \(opponentPlay!) accepted.")
                    
                    // Only process once.
                    opponentPlay = nil
                    startNewTurn()
                }
            }
        }
    }
    
    // Online Mode
    fileprivate func sendGameTurnPacket(targetGrid: GridCoordinate) {
        if self.gkMatch == nil {
            print("GKMatch does not exist.")
            return
        }
        
        let gameTurnPacket = PlayerTurnPacket(turnNum: currentTurnNum, col: targetGrid.col, row: targetGrid.row)
        let data = PacketHelper.encodePlayerTurnPacket(packet: gameTurnPacket)
        do {
            print("Sending game turn message.")
            try gkMatch?.sendData(toAllPlayers: data, with: GKMatchSendDataMode.reliable)
        } catch {
            print("Failed to send game turn message.")
            gkMatch?.disconnect()
        }
    }
    
    fileprivate func startNewTurn() {
        currentTurnNum += 1
        currentTurnTimeRemains = GameConstants.GAME_TURN_TIME
    }
    
    func onelineGameOver(_ selfWin: Bool?) {
        if !gameEnabled {
            // Game already over.
            return
        }
        
        self.touchDisabled = true
        gameEnabled = false
        
        if selfWin == nil {
            // No one win the game.
            return
        }
        
        print("Online game over. I win: \(selfWin!)")
        
        // Calculate data change.
        var scoreChange:Int = 0
        var winChange:Int = 0
        var loseChange:Int = 0
        
        let selfPlayer = gameCore!.getSelfPlayer()
        let oppoPlayer = gameCore!.getOpponentPlayer()
        let scoreDiff = oppoPlayer.playerScore-selfPlayer.playerScore
        
        if selfWin! {
            scoreChange = max(min(10, scoreDiff/10), 3)  // 3 ~ 10
            winChange = 1
        } else {
            scoreChange = min(max(-10, scoreDiff/10), -1)  // -1 ~ -10
            loseChange = 1
        }
        
        updateLeaderBoard(selfPlayer: selfPlayer, deltaScore: scoreChange, deltaWin: winChange, deltaLose: loseChange)
        
        // Show game result
        var resultText = "You"
        var scoreText = "Score: \(selfPlayer.playerScore)"
        var winText = "Win: \(selfPlayer.playerWinNum)"
        var loseText = "Lose: \(selfPlayer.playerLostNum)"
        
        if selfWin! {
            resultText += " Win"
            scoreText += " + \(scoreChange)"
            winText += " + \(winChange)"
        } else {
            resultText += " Lose"
            scoreText += " - \(abs(scoreChange))"
            loseText += " + \(loseChange)"
        }
        showOnlineGameResult(resultText: resultText, scoreText: scoreText, winNumText: winText, loseNumText: loseText)
    }
    
    fileprivate func updateLeaderBoard(selfPlayer:GamePlayer, deltaScore:Int, deltaWin:Int, deltaLose:Int) {
        let localPlayer = GKLocalPlayer.localPlayer()
        if (localPlayer.isAuthenticated) {
            let gcScore = GKScore(leaderboardIdentifier: GameConstants.SCORE_LEADER_BOARD_ID)
            let gcWinNum = GKScore(leaderboardIdentifier: GameConstants.WIN_LEADER_BOARD_ID)
            let gcLostNum = GKScore(leaderboardIdentifier: GameConstants.LOSE_LEADER_BOARD_ID)
            
            gcScore.value = Int64(selfPlayer.playerScore + deltaScore)
            gcWinNum.value = Int64(selfPlayer.playerWinNum + deltaWin)
            gcLostNum.value = Int64(selfPlayer.playerLostNum + deltaLose)

            print("Updating leadearboard...")
            GKScore.report([gcScore, gcWinNum, gcLostNum], withCompletionHandler: {(error: Error?) -> Void in
                if error != nil {
                    print(error!.localizedDescription)
                } else {
                    print("Score submitted to Leaderboards! score = \(gcScore.value), win = \(gcWinNum.value), lose = \(gcLostNum.value)")
                }
            })
        }
    }
    
    fileprivate func showOnlineGameResult(resultText:String, scoreText:String, winNumText:String, loseNumText:String) {
        DispatchQueue.main.async {
            self.popOutNotifier!.titleLabel.text = resultText
            self.popOutNotifier!.detailLabel1.text = scoreText
            self.popOutNotifier!.detailLabel2.text = winNumText
            self.popOutNotifier!.detailLabel3.text = loseNumText
            self.popOutNotifier!.isHidden = false
        }
    }
}
