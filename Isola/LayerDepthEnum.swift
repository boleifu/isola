//
//  LayerDepthEnum.swift
//  Isola
//
//  Created by Bolei Fu on 10/6/15.
//  Copyright © 2015 boleifu. All rights reserved.
//

import SpriteKit

//
// This defines the depth of each object in the game scene
//
enum LayerDepth : Double {
    case background = 1;
    case hudboard = 2;
    case gameboard = 3;
    case gameboardgrid = 4;
    case gridaddon = 5;
    case hudtext = 6;
    case popupnotifier = 98;
    case other = 99;
}
