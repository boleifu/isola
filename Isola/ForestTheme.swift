//
//  ForestTheme.swift
//  Isola
//
//  Created by Bolei Fu on 10/17/15.
//  Copyright © 2015 boleifu. All rights reserved.
//

class ForestTheme: Theme {
    var name:String = "Forest";
    
    // BG image
    var topBackGroundImageName:String = "colored_forest";
    var bottomBackGroundImageName:String = "uncolored_plain";
    
    // Grid Image
    var baseTextureImageName:String = "land_rounded_corners";
    var removedTextureImageName:String = "water_rounded_corners";
    var idelHilightTextureImageName:String = "grey_boxCross";
    var p1MoveHilightTextureImageName:String = "red_circle";
    var p2MoveHilightTextureImageName:String = "blue_circle";
    
    // Game Sound
    var gameOverSoundName:String = "Sounds/jingles_SAX12.wav";
    var moveSoundName:String = "Sounds/footstep07.wav";
    var removeSoundName:String = "Sounds/bookPlace3.wav";
    var invalidMoveSoundName:String = "Sounds/error.wav";
}
